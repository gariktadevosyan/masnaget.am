<?php

use Illuminate\Database\Seeder;
use App\ActivitySphere;

class ActivitySpheresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect(__('activity_spheres'))
            ->sort()
            ->each(function ($sphere, $key) {
                (new ActivitySphere(['title' => $key]))->save();
            });
    }
}
