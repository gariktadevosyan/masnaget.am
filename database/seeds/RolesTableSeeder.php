<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            'administrator',
            'employee',
            'employer',
            'school',
            'master'
        ])->each(function ($title) {
            $role = new Role(['title' => $title]);
            $role->save();
        });
    }
}
