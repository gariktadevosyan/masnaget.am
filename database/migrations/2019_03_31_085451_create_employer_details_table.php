<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployerDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employer_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('name', 50);
            $table->string('type');
            $table->unsignedInteger('activity_sphere_id');
            $table->string('logo', 20);
            $table->string('tin', 20);
            $table->unsignedInteger('employees_count')->nullable();
            $table->unsignedInteger('community_id')->nullable();
            $table->string('address', 100)->nullable();
            $table->string('phone', 100)->nullable();
            $table->string('email', 50);
            $table->string('website', 50)->nullable();
            $table->string('agent', 50)->nullable();
            $table->text('about')->nullable();


            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('activity_sphere_id')->references('id')->on('activity_spheres');
            $table->foreign('community_id')->references('id')->on('communities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employer_details');
    }
}
