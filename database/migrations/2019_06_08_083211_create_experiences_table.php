<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experiences', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cv_id');
            $table->unsignedInteger('sphere_id');
            $table->string('organization');
            $table->string('position');
            $table->date('start_date');
            $table->date('end_date');

            $table->foreign('cv_id')->references('id')->on('curriculum_vitaes');
            $table->foreign('sphere_id')->references('id')->on('activity_spheres');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experiences');
    }
}
