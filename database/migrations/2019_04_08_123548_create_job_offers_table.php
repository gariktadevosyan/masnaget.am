<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_offers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('activity_sphere_id');
            $table->unsignedInteger('community_id')->nullable();
            $table->string('title', 100);
            $table->text('description')->nullable();
            $table->enum('term', [0, 1, 2]);
            $table->enum('type', [0, 1, 2]);
            $table->string('address', 100)->nullable();
            $table->date('opening_date');
            $table->date('deadline');
            $table->string('languages', 50);
            $table->enum('experience', [0, 1, 2]);
            $table->enum('military_book', [0, 1, 2]);
            $table->enum('drive_right', [0, 1, 2]);
            $table->enum('education', [0, 1, 2]);
            $table->text('skills')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('activity_sphere_id')->references('id')->on('activity_spheres');
            $table->foreign('community_id')->references('id')->on('communities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_offers');
    }
}
