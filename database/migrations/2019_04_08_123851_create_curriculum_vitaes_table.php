<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurriculumVitaesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curriculum_vitaes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('community_id')->nullable();
            $table->string('name', 100);
            $table->string('avatar', 20)->nullable();
            $table->string('phone', 100)->nullable();
            $table->date('birth_date');
            $table->string('address', 100)->nullable();
            $table->enum('education', [0, 1, 2]);
            $table->text('education_details')->nullable();
            $table->string('languages', 50);
            $table->text('skills')->nullable();
            $table->boolean('military_book')->default(false);
            $table->boolean('drive_right')->default(false);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('community_id')->references('id')->on('communities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curriculum_vitaes');
    }
}
