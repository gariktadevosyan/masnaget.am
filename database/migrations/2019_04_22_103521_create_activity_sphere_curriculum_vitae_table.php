<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitySphereCurriculumVitaeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_sphere_curriculum_vitae', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('curriculum_vitae_id');
            $table->unsignedInteger('activity_sphere_id');
            $table->enum('experience', [0, 1, 2]);

            $table->foreign('curriculum_vitae_id')->references('id')->on('curriculum_vitaes');
            $table->foreign('activity_sphere_id')->references('id')->on('activity_spheres');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_sphere_curriculum_vitae');
    }
}
