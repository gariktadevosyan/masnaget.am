$(document).off('.datepicker.data-api');

$('[data-provide="datepicker"]').datepicker({
    language: document.getElementsByTagName("HTML")[0].lang,
    autoclose: true
});

$('[data-provide="yearpicker"]').datepicker({
    autoclose: true,
    format: " yyyy",
    viewMode: "years",
    minViewMode: "years"
});

$('[data-provide="monthyearpicker"]').datepicker({
    language: document.getElementsByTagName("HTML")[0].lang,
    autoclose: true,
    format: " mm.yyyy",
    viewMode: "months",
    minViewMode: "months"
});