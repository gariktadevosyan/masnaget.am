$('[data-provider="repeater"]').each(function () {
    const $el = this;
    const $repeater = $(this).repeater({
        show: function () {
            const $this = $(this);

            $this.slideDown();

            const inputs = $this.find('[name]');

            if (!$el.dataset.setlist && this.previousSibling) {
                inputs[0].focus();
            }

            inputs.each(function () {
                if (this.defaultValue && !this.value.length) {
                    this.value = this.defaultValue;
                }

                if (this.nodeName === 'SELECT' && this.value === '') {
                    this.options[0].selected = true;
                }
            });

            const datePickers = $this.find('[data-provide="datepicker"]');
            if (datePickers.length) {
                datePickers.datepicker({
                    language: document.getElementsByTagName("HTML")[0].lang,
                    autoclose: true
                });
            }

            const yearPickers = $this.find('[data-provide="yearpicker"]');
            if (yearPickers.length) {
                yearPickers.datepicker({
                    autoclose: true,
                    format: " yyyy",
                    viewMode: "years",
                    minViewMode: "years"
                });
            }

            const monthYearPickers = $this.find('[data-provide="monthyearpicker"]');
            if (monthYearPickers.length) {
                monthYearPickers.datepicker({
                    language: document.getElementsByTagName("HTML")[0].lang,
                    autoclose: true,
                    format: " mm.yyyy",
                    viewMode: "months",
                    minViewMode: "months"
                });
            }
        },
        hide: function (deleteElement) {
            $(this).slideUp(deleteElement);
        }
    });

    if (this.dataset.setlist) {
        $repeater.setList(JSON.parse(this.dataset.setlist));
    }
});


