$('#jobOfferList').DataTable(Object.assign(DTSettings, {
    ajax: '/admin/job-offers',
    responsive: false,
    order: [[ 3, "desc" ]],
    columns: [
        {
            name: 'title',
            data: 'title'
        },
        {
            name: 'opening_date',
            data: 'opening_date'
        },
        {
            name: 'deadline',
            data: 'deadline'
        },
        {
            name: 'updated_at',
            data: 'updated_at'
        },
        {
            orderable: false,
            data: 'id',
            className: 'text-center text-nowrap',
            render: function (data) {
                return '<a href="/admin/job-offers/'+ data +'/edit" class="btn btn-sm btn-warning mr-1"><i class="fa fa-pencil"></i></a>' +
                    '<button type="button" class="btn btn-sm btn-secondary" data-provider="confirmation" data-id="'+ data +'"><i class="fa fa-trash"></i></button>';
            }
        }
    ],
    drawCallback: function (settings) {
        const DT = this;
        $(settings.nTBody).find('[data-provider="confirmation"]')
            .confirmation({
                title: 'Արխիվացնե՞լ գրառումը',
                singleton: true,
                popout: true,
                rootSelector: '[data-provider="confirmation"]',
                copyAttributes: 'data-id',
                btnOkLabel: '',
                btnOkClass: 'btn btn-sm btn-outline-success',
                btnOkIconClass: 'fa fa-check',
                btnCancelLabel: '',
                btnCancelClass: 'btn btn-sm btn-outline-danger',
                btnCancelIconClass: 'fa fa-close',
                onConfirm: function () {
                    axios.delete('/admin/job-offers/' + this.dataset.id)
                        .then(() => {
                            DT.api().table().ajax.reload();
                            Toastr.success('Գրառումն արխիվացված է');
                            const ArchiveCounterBadge = document.getElementById('archiveCounterBadge');
                            ArchiveCounterBadge.innerText = +ArchiveCounterBadge.innerText + 1;
                        });
                }
            });
    }
}));