$('#jobOfferArchiveList').DataTable(Object.assign(DTSettings, {
    ajax: '/admin/job-offers/archive',
    order: [[ 3, "desc" ]],
    columns: [
        {
            name: 'title',
            data: 'title'
        },
        {
            name: 'opening_date',
            data: 'opening_date'
        },
        {
            name: 'deadline',
            data: 'deadline'
        },
        {
            name: 'deleted_at',
            data: 'deleted_at'
        },
        {
            orderable: false,
            data: 'id',
            className: 'text-center text-nowrap',
            render: function (data) {
                    return '<button type="button" class="btn btn-sm btn-secondary" data-provider="confirmation" data-id="'+ data +'"><i class="fa fa-copy"></i></button>';
            }
        }
    ],
    drawCallback: function (settings) {
        const DT = this;
        $(settings.nTBody).find('[data-provider="confirmation"]')
            .confirmation({
                title: 'Վերականգնե՞լ կրկնօրինակը',
                singleton: true,
                popout: true,
                rootSelector: '[data-provider="confirmation"]',
                copyAttributes: 'data-id',
                btnOkLabel: '',
                btnOkClass: 'btn btn-sm btn-outline-success',
                btnOkIconClass: 'fa fa-check',
                btnCancelLabel: '',
                btnCancelClass: 'btn btn-sm btn-outline-danger',
                btnCancelIconClass: 'fa fa-close',
                onConfirm: function () {
                    axios.patch('/admin/job-offers/' + this.dataset.id + '/restore')
                        .then((response) => {
                            location.href = '/admin/job-offers/' + response.data + '/edit';
                        });
                }
            });
    }
}));