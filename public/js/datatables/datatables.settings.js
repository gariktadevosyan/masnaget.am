const DTSettings = {
    responsive: true,
    serverSide: true,
    language: {
        processing: "Բեռնվում է...",
        search: "Փնտրել",
        lengthMenu:    "Ցուցադրել _MENU_ գրառում",
        info:           "_START_ - _END_ գրառումներ: Ընդամենը _TOTAL_",
        infoEmpty:      "0 գրառում",
        infoFiltered:   "(ֆիլտրված _MAX_ գրառումից)",
        infoPostFix:    "",
        loadingRecords: "Բեռնվում է...",
        zeroRecords:    "Համապատասխան գրառումներ չկան",
        emptyTable:     "Ցուցադրման ենթակա գրառումներ չկան",
        paginate: {
            first:      "Առաջին",
            previous:   "Նախորդը",
            next:       "Հաջորդը",
            last:       "Վերջին"
        },
        aria: {
            sortAscending:  ": activer pour trier la colonne par ordre croissant",
            sortDescending: ": activer pour trier la colonne par ordre décroissant"
        }
    }
};