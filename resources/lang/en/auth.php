<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'email' => 'Email',
    'login' => 'Login',
    'password' => 'Password',
    'stay_logged_in' => 'Stay logged in',
    'forgot_password' => 'forgot password?',
    'register' => 'Don\'t have account yet? Create one!',
    'reset_password' => 'Reset password',
    'new_password' => 'new password',
    'request' => 'Request new password',
    'change_password' => 'Change password',
    'password_confirmation' => 'Confirm password',
    'already_registered' => 'Already registered?',
    'register_as' => 'Register as',
    'verify' => 'Email verification needed',
    'verify_email' => 'We have emailed your verification link.',
    'check_inbox' => 'Check your inbox please for verification link.',
    'reset' => 'If there is no email in your inbox from us, click here!',
    'registered' => 'We have sent verification link to :email. Verify your profile for the full functionality.',
    'back' => 'back',
    'register_submit' => 'register',
];
