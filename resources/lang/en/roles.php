<?php

return [
    'employee' => 'Employee',
    'employer' => 'Employer',
    'school' => 'School',
    'master' => 'Master',
];