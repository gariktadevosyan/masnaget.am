<?php

return [
    'close' => 'close',
    'log_out' => 'Logout',
    'administration' => 'Administration',
    'my_cv' => 'My CV',
    'home' => 'Home'
];