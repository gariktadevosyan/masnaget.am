<?php

return [
    'close' => 'փակել',
    'log_out' => 'ելք',
    'administration' => 'Կառավարում',
    'my_cv' => 'Իմ ինքնակենսագրականը',
    'home' => 'Գլխավոր',
    'saved' => 'Փոփոխությունները հաջողությամբ պահպանված են',
    'save' => 'Պահպանել',
    'log_in_to_apply' => 'Դիմելու համար անհրաժեշտ է <a href="#" data-toggle="modal" data-target="#loginModal">մուտք գործել</a>',
    'fill_cv_to_apply' => 'Դիմելու համար անհրաժեշտ է <a href="'. route('cv.index') .'" target="_blank">լրացնել ինքնակենսագրականը</a>',
    'applicationSuccess' => 'Դիմումն ուղարկված է'
];