<?php

return [
    'name' => 'ԱԱՀ',
    'birth_date' => 'Ծննդյան ամսաթիվ',
    'military_book' => 'Զին․ գրքույկ',
    'available' => 'առկա է',
    'drive_right' => 'Վարորդական իրավունք',
    'desired_sphere' => 'Փնտրվող աշխատանքի ոլորտը',
    'sphere_experience' => 'Փորձը ոլորտում',
    'education' => 'Կրթություն',
    'experience' => 'Աշխատանքային փորձ',
    'organization' => 'Կազմակերպություն',
    'start_date' => 'Սկիզբ',
    'end_date' => 'Ավարտ',
    'sphere' => 'Ոլորտը',
    'position' => 'Պաշտոնը',
    'languages' => 'Լեզուների իմացություն',
    'skills' => 'Մասնագիտական հմտություններ',
    'upload_image' => 'Բեռնել նկար',
    'place' => 'Բնակության վայր',
    'institution' => 'Հաստատություն',
    'profession' => 'Մասնագիտություն',
    'languageRequired' => 'Նվազագույնը մեկ լեզու',
    'required_warning' => '<span class="text-danger">*</span>-ով նշված դաշտերը պարտադիր են լրացման',
    'apply' => 'Դիմել',
    'phone' => 'Հեռախոս'
];