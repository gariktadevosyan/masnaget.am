<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Սխալ օգտանուն կամ գաղտնաբառ։',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'email' => 'Էլ․ փոստ',
    'login' => 'Մուտք',
    'password' => 'Գաղտնաբառ',
    'stay_logged_in' => 'Մնալ համակարգում',
    'forgot_password' => 'մոռացե՞լ եք գաղտնբառը',
    'register' => 'Դեռ չե՞ք գրանցվել։',
    'reset_password' => 'Փոխել գաղտնաբառը',
    'new_password' => 'Նոր գաղտնաբառ',
    'request' => 'Ուղարկել նոր գաղտնաբառի հարցում',
    'change_password' => 'Փոխել գաղտնաբառը',
    'password_confirmation' => 'Հաստատեք գաղտնաբառը',
    'already_registered' => 'Արդեն գրանցվե՞լ եք',
    'register_as' => 'Գրանցվել որպես',
    'verify' => 'Անհրաժեշտ է հաստատել գրանցումը։',
    'verify_email' => 'Անհրաժեշտ է հաստատել գրանցումը։',
    'check_inbox' => 'Հաստատման հղումը ուղարկվել է ձեր էլ փոստին։',
    'reset' => 'Եթե նամակ չեք ստացել, սեղմեք այստեղ։',
    'registered' => 'Մենք ուղարկել ենք հաստատման հղումը :email էլ․ փոստին։ Խնդրում ենք հաստատել գրանցումը կայքի բոլոր հնարավորություններից օգտվելու համար։',
    'back' => 'վերադռնալ',
    'register_submit' => 'գրանցվել',
];
