<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Գաղտաբառը պետք է լինի նվազագույնը 6 նիշ և համապատասխանի հաստատմանը։',
    'reset' => 'Գաղտնաբառը հաջողությամբ փոխված է։',
    'sent' => 'Մենք ուղարկեցինք գաղտնաբառի փոփոխման հղումը ձեր էլ․ փոստին։',
    'token' => 'Հղումը ժամկետանց է, խնդրում ենք ձևակերպել նոր հարցում։',
    'user' => 'Անհայտ էլ․ փոստ',

];
