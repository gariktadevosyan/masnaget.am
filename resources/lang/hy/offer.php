<?php

return [
    'term' => 'Տևողություն',
    'type' => 'Դրույք',
    'opening_date' => 'Սկիզբ',
    'deadline' => 'Վերջնաժամկետ',
    'updated_at' => 'Թարմացվել է`',
    'employees_count' => 'Աշխատակիցների քանակը',
    'tin' => 'ՀՎՀՀ'
];