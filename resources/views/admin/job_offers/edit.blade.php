@extends('layouts.admin')
@push('styles')
    <link rel="stylesheet" href="{{ asset('theme/vendor/bootstrap-datepicker/bootstrap-datepicker3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bs-stepper.css') }}" media="all">
@endpush
@section('content')
    @breadcrumb(['data' => [
        [
            'հայտարարություններ', route('job-offers.index')],
            'խմբագրել հայտարարությունը'
        ]
    ])

    <div class="card">
        <div class="card-body">
            <div data-provider="stepper" class="bs-stepper">
                <div class="bs-stepper-header">
                    <div class="step" data-target="#description_step">
                        <a href="#">
                            <span class="bs-stepper-circle">1</span>
                            <span class="bs-stepper-label">Նկարագրություն</span>
                        </a>
                    </div>
                    <div class="line"></div>
                    <div class="step" data-target="#requirements_step">
                        <a href="#">
                            <span class="bs-stepper-circle">2</span>
                            <span class="bs-stepper-label">Պահանջներ</span>
                        </a>
                    </div>
                </div>
                <div class="bs-stepper-content">
                    <form action="{{ route('job-offers.update', [$offer->id]) }}" method="post">
                        @csrf
                        @method('PUT')
                        <div id="description_step" class="content">
                            <div class="form-group">
                                <label for="title" class="required">Անվանում</label>
                                <input
                                        type="text"
                                        class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}"
                                        id="title"
                                        name="title"
                                        value="{{ old('title') ?: $offer->title }}"
                                >
                            </div>
                            <div class="form-group">
                                <label for="sphere">Ոլորտ</label>
                                <select name="activity_sphere_id" id="sphere" class="form-control">
                                    @foreach ($spheres as $sphere)
                                        <option
                                                value="{{ $sphere->id }}"
                                                {{ old('activity_sphere_id') === (string)$sphere->id || $offer->activity_sphere_id === $sphere->id ? 'selected' : '' }}
                                        >{{ $sphere->title }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="description">Նկարագրություն</label>
                                <textarea name="description" data-provider="tinymce" class="autoheight">{{old('description') ?: $offer->description}}</textarea>
                            </div>
                            <div class="form-group">
                                <label>@lang('offer.term')</label>
                                <div>
                                    @foreach (__('options.terms') as $key => $value)
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input
                                                    type="radio"
                                                    id="term_{{ $key }}"
                                                    name="term"
                                                    value="{{ $key }}"
                                                    class="custom-control-input"
                                                    {{ old('term') === (string)$key || (string)$key === $offer->term ? 'checked' : '' }}
                                            >
                                            <label class="custom-control-label" for="term_{{ $key }}">{{ $value }}</label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group">
                                <label>@lang('offer.type')</label>
                                <div>
                                    @foreach (__('options.types') as $key => $value)
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input
                                                    type="radio"
                                                    id="type_{{ $key }}"
                                                    name="type"
                                                    value="{{ $key }}"
                                                    class="custom-control-input"
                                                    {{ old('type') === (string)$key || (string)$key === $offer->type ? 'checked' : '' }}
                                            >
                                            <label class="custom-control-label" for="type_{{ $key }}">{{ $value }}</label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Վայրը</label>
                                <place-select old="{{ old('community_id') ?: $offer->community_id }}" address="{{ old('address') ?: $offer->address }}" {{ is_null($offer->community_id) ? 'outgoing' : '' }}></place-select>
                            </div>
                            <div class="form-group">
                                <label for="opening_date">@lang('offer.opening_date')</label>
                                <input
                                        type="text"
                                        class="form-control {{ $errors->has('opening_date') ? 'is-invalid' : '' }}"
                                        id="opening_date"
                                        name="opening_date"
                                        value="{{ old('opening_date') ?: $offer->opening_date }}"
                                        data-provide="datepicker"
                                        autocomplete="off"
                                >
                            </div>
                            <div class="form-group">
                                <label for="deadline">@lang('offer.deadline')</label>
                                <input
                                        type="text"
                                        class="form-control {{ $errors->has('deadline') ? 'is-invalid' : '' }}"
                                        id="deadline"
                                        name="deadline"
                                        value="{{ old('deadline') ?: $offer->deadline }}"
                                        data-provide="datepicker"
                                        autocomplete="off"
                                >
                            </div>
                            <div class="text-right">
                                <button type="button" class="btn btn-primary" onclick="myStepper.next()">Հաջորդը</button>
                            </div>
                        </div>
                        <div id="requirements_step" class="content">
                            <div class="form-group">
                                <label>@lang('cv.education')</label>
                                <div>
                                    @foreach (__('options.education') as $key => $value)
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio"
                                                   id="education_{{ $key }}"
                                                   name="education"
                                                   value="{{ $key }}"
                                                   class="custom-control-input"
                                                   @if (old('education') === (string)$key || (string)$key === $offer->education) checked @endif
                                            >
                                            <label class="custom-control-label" for="education_{{ $key }}">{{ $value }}</label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group">
                                <label>@lang('cv.experience')</label>
                                <div>
                                    @foreach (__('options.experiences') as $key => $value)
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio"
                                                   id="experience_{{ $key }}"
                                                   name="experience"
                                                   value="{{ $key }}"
                                                   class="custom-control-input"
                                                   @if (old('experience') === (string)$key || (string)$key === $offer->experience) checked @endif
                                            >
                                            <label class="custom-control-label" for="experience_{{ $key }}">{{ $value }}</label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group">
                                <label>@lang('cv.languages')</label>
                                <div>
                                    @foreach(__('options.languages') as $key => $lang)
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox"
                                                   class="custom-control-input"
                                                   name="languages[]"
                                                   id="languge_{{ $key }}"
                                                   value="{{ $key }}"
                                                   @if ((is_array(old('languages')) && in_array($key, old('languages'))) || (!is_array(old('languages')) && in_array($key, $offer->languages)))
                                                   checked
                                                    @endif
                                            >
                                            <label class="custom-control-label" for="languge_{{ $key }}">{{ $lang }}</label>
                                        </div>
                                    @endforeach
                                </div>
                                <span class="form-text text-danger">{{ $errors->has('languages') ? 'Նվազագույնը մեկ լեզու' : '' }}</span>
                            </div>
                            <div class="form-group">
                                <label for="required_qualifications">@lang('cv.skills')</label>
                                <textarea name="skills" data-provider="tinymce" class="autoheight">{{old('skills') ?: $offer->skills}}</textarea>
                            </div>
                            <div class="form-group">
                                <label>@lang('cv.military_book')</label>
                                <div>
                                    <button-switch options="{{ json_encode(__('options.military_book')) }}" name="military_book" checked="{{ old('military_book') ?: $offer->military_book }}"></button-switch>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>@lang('cv.drive_right')</label>
                                <div>
                                    <button-switch options="{{ json_encode(__('options.drive_right')) }}" name="drive_right" checked="{{ old('drive_right') ?: $offer->drive_right }}"></button-switch>
                                </div>
                            </div>
                            <div class="text-right">
                                <button type="button" class="btn btn-primary" onclick="myStepper.previous()">Նախորդը</button>
                                <button type="submit" class="btn btn-success">Հաստատել</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <p class="text-right">@lang('cv.required_warning')</p>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{ asset('theme/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('theme/vendor/bootstrap-datepicker/bootstrap-datepicker.hy.min.js') }}"></script>
    <script src="{{ asset('theme/vendor/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('js/tinymce.init.js') }}"></script>
    <script src="{{ asset('js/stepper.init.js') }}"></script>
@endpush