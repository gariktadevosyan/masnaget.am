@extends('layouts.admin')
@push('styles')
    <link rel="stylesheet" href="{{ asset('theme/vendor/DataTables-1.10.18/datatables.min.css') }}">
@endpush
@section('content')
    <div class="mb-2">
        <a href="{{ route('job-offers.create') }}" class="btn btn-primary">
            Ստեղծել հայտարարություն
        </a>
        <a href="{{ route('job-offers.archive') }}" class="btn btn-secondary">
            Արխիվ
            <span class="badge badge-light" id="archiveCounterBadge">{{ $archiveCount }}</span>
        </a>
    </div>
    <div class="card">
        <div class="card-body">
            <table class="table table-hover w-100" id="jobOfferList">
                <thead>
                    <tr>
                        <th>Անվանում</th>
                        <th>@lang('offer.opening_date')</th>
                        <th>@lang('offer.deadline')</th>
                        <th>Թարմացվել է</th>
                        <td class="text-center">
                            <i class="fa fa-cogs"></i>
                        </td>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{ asset('theme/vendor/DataTables-1.10.18/datatables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/datatables.settings.js') }}"></script>
    <script src="{{ asset('js/datatables/jobOfferList.init.js') }}"></script>
@endpush