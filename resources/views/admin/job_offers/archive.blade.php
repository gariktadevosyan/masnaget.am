@extends('layouts.admin')
@push('styles')
    <link rel="stylesheet" href="{{ asset('theme/vendor/DataTables-1.10.18/datatables.min.css') }}">
@endpush
@section('content')
    @breadcrumb(['data' => [
    ['հայտարարություններ', route('job-offers.index')],
    'արխիվ'
    ]])
    <div class="card">
        <div class="card-body">
            <table class="table table-hover w-100" id="jobOfferArchiveList">
                <thead>
                <tr>
                    <th>Անվանում</th>
                    <th>@lang('offer.opening_date')</th>
                    <th>@lang('offer.deadline')</th>
                    <th>Արխիվացվել է</th>
                    <td class="text-center">
                        <i class="fa fa-cogs"></i>
                    </td>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{ asset('theme/vendor/DataTables-1.10.18/datatables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/datatables.settings.js') }}"></script>
    <script src="{{ asset('js/datatables/jobOfferArchiveList.init.js') }}"></script>
@endpush