@extends('layouts.admin')
@section('content')
    <div class="card">
        <div class="card-body">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" data-toggle="tab" href="#general_tab" role="tab">Ընդհանուր</a>
                    <a class="nav-item nav-link" data-toggle="tab" href="#address_tab" role="tab">Հասցե</a>
                    <a class="nav-item nav-link" data-toggle="tab" href="#contact_tab" role="tab">Կոնտակտներ</a>
                    <a class="nav-item nav-link" data-toggle="tab" href="#about_tab" role="tab">Կազմակերպության մասին</a>
                </div>
            </nav>

            <form action="{{ route('settings.update') }}" method="post">
                @csrf
                @method('PUT')
                <div class="tab-content">
                    <div class="tab-pane fade show active pt-2" id="general_tab" role="tabpanel">
                        <div class="form-group">
                            <div class="form-group">
                                <img-upload name="logo" required old="{{old('logo')}}" initial="{{$settings->logo}}" title="Բեռնել նկար"></img-upload>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="required">Անվանում</label>
                            <input type="text" id="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" maxlength="50" value="{{ old('name') ?: $settings->name }}">
                        </div>
                        <div class="form-group">
                            <label for="type">Տեսակ</label>
                            <select id="type" class="form-control">
                                <option value=""></option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="activity_sphere">Գործունեության ոլորտը</label>
                            <select name="activity_sphere_id" id="activity_sphere" class="form-control">
                                @foreach($activitySpheres as $sphere)
                                    <option value="{{ $sphere->id }}" {{ $settings->activity_sphere_id === $sphere->id ? 'selected' : '' }}>{{ $sphere->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="tin" class="required">ՀՎՀՀ</label>
                            <input type="text" id="tin" class="form-control {{ $errors->has('tin') ? 'is-invalid' : '' }}" name="tin" placeholder="00000000" data-inputmask="'mask': '99999999'" value="{{ old('tin') ?: $settings->tin }}">
                        </div>
                        <div class="form-group">
                            <label for="employees_count">Աշխատակիցների քանակը</label>
                            <input type="number" id="employees_count" class="form-control" name="employees_count" min="0" value="{{ $settings->employees_count }}">
                        </div>
                    </div>
                    <div class="tab-pane fade pt-2" id="address_tab" role="tabpanel">
                        <place-select old="{{ old('community_id') ?: $settings->community_id }}" address="{{ old('address') ?: $settings->address }}" {{ is_null($settings->community_id) ? 'outgoing' : '' }}></place-select>
                    </div>
                    <div class="tab-pane fade pt-2" id="contact_tab" role="tabpanel">
                        <div class="form-group">
                            <label>Հեռախոս</label>
                            <div
                                    data-provider="repeater"
                                    data-setlist="@if (is_array(old('phone'))) {{json_encode(old('phone'))}} @elseif(is_array($settings->phone)) {{json_encode($settings->phone)}} @endif"
                            >
                                <div data-repeater-list="phone">
                                    <div data-repeater-item>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" name="phone" placeholder="+374" value="+374">
                                            <div class="input-group-append">
                                                <button type="button" class="btn btn-danger" data-repeater-delete>
                                                    <i class="fa fa-minus"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button type="button" class="btn btn-primary" data-repeater-create>
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email">Էլ․ փոստ</label>
                            <input type="email" id="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" name="email" value="{{ old('email') ?: $settings->email }}">
                        </div>
                        <div class="form-group">
                            <label for="website">Կայք</label>
                            <input type="text" id="website" class="form-control" name="website" value="{{ $settings->website }}">
                        </div>
                        <div class="form-group">
                            <label for="agent_name">Ներկայացուցիչ</label>
                            <input type="text" id="agent_name" class="form-control" name="agent" placeholder="Անուն / Ազգանուն" value="{{ $settings->agent }}">
                        </div>
                    </div>
                    <div class="tab-pane fade pt-2" id="about_tab" role="tabpanel">
                        <div class="form-group">
                            <textarea name="about" data-provider="tinymce" class="fullpage">{{$settings->about}}</textarea>
                        </div>
                    </div>
                    <div class="text-right">
                        <button class="btn btn-primary">Պահպանել</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{ asset('js/jquery.repeater.init.js') }}"></script>
    <script src="{{ asset('theme/vendor/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('js/tinymce.init.js') }}"></script>
@endpush