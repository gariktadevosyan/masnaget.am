<a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="{{ $navLink ? 'nav-link' : '' }} dropdown-toggle">
    <img src="{{ asset('img/flag_'. App::getLocale() .'.png') }}">
</a>
<div class="dropdown-menu mw-auto">
    @if(!App::isLocale('hy'))
        <a href="{{ route('locale', ['hy']) }}" class="dropdown-item">
            <img src="{{ asset('img/flag_hy.png') }}">
        </a>
    @endif
    @if(!App::isLocale('en'))
        <a href="{{ route('locale', ['en']) }}" class="dropdown-item">
            <img src="{{ asset('img/flag_en.png') }}">
        </a>
    @endif
</div>