<li class="nav-item dropdown">
    <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">
        {{ $user->email }}
    </a>
    <div class="dropdown-menu">
        <button type="button"
                class="dropdown-item"
                onclick="document.getElementById('logoutForm').submit()"
        >
            <i class="fa fa-power-off text-danger mr-2"></i> @lang('client.log_out')
        </button>
    </div>
</li>
<form action="{{ route('logout') }}" method="post" id="logoutForm">@csrf</form>