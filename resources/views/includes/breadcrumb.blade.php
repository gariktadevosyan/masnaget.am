<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        @foreach($data as $breadcrumb)
            @if(is_array($breadcrumb))
                <li class="breadcrumb-item">
                    <a href="{{ $breadcrumb[1] }}">{{ $breadcrumb[0] }}</a>
                </li>
                @continue
            @endif
            <li class="breadcrumb-item active" aria-current="page">{{ $breadcrumb }}</li>
        @endforeach
    </ol>
</nav>