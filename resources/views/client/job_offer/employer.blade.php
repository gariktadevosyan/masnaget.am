@extends('layouts.client')
@section('content')
    <header class="text-center border border-left-0 border-right-0 py-3 mb-3">
        <img src="{{ $employer->logo }}" alt="logo" width="100" height="100">
        <h2>{{ $employer->name }}</h2>
        <div>
            <a href="/sphere/{{ $employer->activitySphere->id }}">{{ $employer->activitySphere->title }}</a>:
            <span>{{ $employer->location }}</span>
        </div>
    </header>
    <div class="text-right text-secondary small">
        {{ $employer->type }},
        @lang('offer.employees_count') {{ $employer->employees_count }},
        @lang('offer.tin') {{ $employer->tin }}
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                {!! $employer->about !!}
            </div>
        </div>
    </div>
    <footer class="text-center border border-left-0 border-right-0 py-3 mb-3">
        <ul class="list-inline mb-0">
            <li class="list-inline-item">
                <a target="_blank" href="{{ $employer->website }}"><i class="fas fa-external-link-alt"></i> {{ $employer->website }}</a>
            </li>
            <li class="list-inline-item">
                <a href="mailto:{{$employer->email}}" target="_top"><i class="fas fa-envelope"></i> {{ $employer->email }}</a>
            </li>
            <li class="list-inline-item">
                <i class="fas fa-phone text-primary"></i>
                @foreach($employer->phone as $phone)
                    <a href="tel:{{$phone['phone']}}">
                        {{ $phone['phone'] }}
                    </a>
                @endforeach
                <span class="text-primary">({{ $employer->agent }})</span>
            </li>
        </ul>
    </footer>
    <div class="container">
        <job-listing :search="{{ json_encode(['employer' => $employer->user_id]) }}"></job-listing>
    </div>
@endsection