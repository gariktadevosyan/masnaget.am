@extends('layouts.client')
@section('content')
    <div class="container">
        <header class="mb-2">
            <h2 class="mb-0">{{ $offer->title }}</h2>
            <div>
                <a href="/sphere/{{ $offer->activitySphere->id }}">
                    <small>{{ $offer->activitySphere->title }}</small>
                </a>
                <small class="pull-right">
                    {{ $offer->location }}
                </small>
            </div>
        </header>
        <a href="/employer/{{ $offer->employer->employerDetails->id }}" target="_blank">
            <img src="{{ $offer->employer->employerDetails->logo }}" alt="logo">
            <h3>{{ $offer->employer->employerDetails->name }}</h3>
        </a>
        <div class="row">
            <div class="col-lg-4">
                <dl class="row">
                    <dt class="col-lg-5">@lang('offer.term')</dt>
                        <dd class="col-lg-7">@lang('options.terms.'. $offer->term)</dd>
                    <dt class="col-lg-5">@lang('offer.type')</dt>
                        <dd class="col-lg-7">@lang('options.types.'. $offer->type)</dd>
                    <dt class="col-lg-5">@lang('offer.opening_date')</dt>
                        <dd class="col-lg-7">{{ $offer->opening_date }}</dd>
                    <dt class="col-lg-5">@lang('offer.deadline')</dt>
                        <dd class="col-lg-7">{{ $offer->deadline }}</dd>
                </dl>
            </div>
            <div class="col-lg-8">
                {!! $offer->description !!}
                <dl class="row">
                    <dt class="col-lg-5">@lang('cv.education')</dt>
                    <dd class="col-lg-7">@lang('options.education.'. $offer->education)</dd>
                    <dt class="col-lg-5">@lang('cv.experience')</dt>
                    <dd class="col-lg-7">@lang('options.experiences.'. $offer->experience)</dd>
                    <dt class="col-lg-5">@lang('cv.languages')</dt>
                    <dd class="col-lg-7">
                        @foreach($offer->languages as $lang)
                            @lang('options.languages.' . $lang)@continue($loop->last){{', '}}
                        @endforeach
                    </dd>
                    <dt class="col-12">@lang('cv.skills')</dt>
                    <dd class="col-12 border rounded">{!! $offer->skills !!}</dd>
                    <dt class="col-lg-5">@lang('cv.military_book')</dt>
                    <dd class="col-lg-7">@lang('options.military_book.'. $offer->military_book)</dd>
                    <dt class="col-lg-5">@lang('cv.drive_right')</dt>
                    <dd class="col-lg-7">@lang('options.drive_right.'. $offer->drive_right)</dd>
                </dl>
                <p class="text-right text-secondary"><small>@lang('offer.updated_at') {{ $offer->updated_at }}</small></p>
                <div class="text-center">
                    @auth
                        @if(Auth::user()->isRole('employee'))
                            @can('apply')
                                <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#applicationModal">
                                    @lang('cv.apply')
                                </button>
                            @endcan
                            @cannot('apply')
                                    @lang('client.fill_cv_to_apply')
                            @endcannot
                        @endif
                    @endauth
                    @guest
                        @lang('client.log_in_to_apply')
                    @endguest
                </div>
            </div>
        </div>
    </div>

    <!-- Application Modal -->
    <div class="modal fade" id="applicationModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Մոտիվացիոն նամակ</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/api/job-offer/apply/{{ $offer->id }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="why_company">
                                <strong>Ինչու՞ եք դիմել այս կազմակերպություն</strong><br>
                                <small>(առավելագույնը 70 բառ)</small>
                            </label>
                            <textarea name="motivation[company]" id="why_company" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="why_job">
                                <strong>Ինչու՞ եք դիմել այս աշխատատեղին</strong><br>
                                <small>(առավելագույնը 100 բառ)</small>
                            </label>
                            <textarea name="motivation[job]" id="why_job" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="why_you">
                                <strong>Ինչու՞ մենք պետք է ընտրենք Ձեզ</strong><br>
                                <small>(առավելագույնը 100 բառ)</small>
                            </label>
                            <textarea name="motivation[you]" id="why_you" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Հաստատել</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection