@extends('layouts.client')
@push('styles')
    <link rel="stylesheet" href="{{ asset('theme/vendor/bootstrap-datepicker/bootstrap-datepicker3.min.css') }}">
@endpush
@section('content')
<div class="container">
    <form action="{{ route('cv.store') }}" method="post">
        @csrf
        <div class="form-group">
            <img-upload name="avatar" old="{{old('avatar')}}" @if(!is_null($cv)) initial="{{$cv->avatar}}" @endif {{ !is_null($cv) && !is_null($cv->avatar) ? 'deletable' : '' }} title="@lang('cv.upload_image')"></img-upload>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label for="name" class="required">@lang('cv.name')</label>
                    <input type="text"
                           name="name"
                           id="name"
                           class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                           value="@if(!is_null(old('name'))) {{ old('name') }} @elseif(!is_null($cv)) {{ $cv->name }} @endif"
                    >
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="birth_date" class="required">@lang('cv.birth_date')</label>
                    <input
                            type="text"
                            class="form-control {{ $errors->has('birth_date') ? 'is-invalid' : '' }}"
                            id="birth_date"
                            name="birth_date"
                            value="@if(!is_null(old('birth_date'))) {{ old('birth_date') }} @elseif(!is_null($cv)) {{ $cv->birth_date }} @endif"
                            data-provide="datepicker"
                            autocomplete="off"
                    >
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>@lang('cv.phone')</label>
            <div
                    data-provider="repeater"
                    data-setlist="@if (is_array(old('phone'))) {{json_encode(old('phone'))}} @elseif(!is_null($cv) && !empty($cv->phone)) {{json_encode($cv->phone)}} @endif"
            >
                <div data-repeater-list="phone">
                    <div data-repeater-item>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="phone" placeholder="+374" value="+374">
                            <div class="input-group-append">
                                <button type="button" class="btn btn-outline-danger" data-repeater-delete>
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <button type="button" class="btn btn-outline-primary" data-repeater-create>
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>@lang('cv.place')</label>
            <div class="row">
                <div class="col-md-8">
                    <place-select old="@if(!is_null(old('community_id'))){{old('community_id')}}@elseif(!is_null($cv)){{$cv->community_id}}@endif"
                                  {{ !is_null($cv) && is_null($cv->community_id) ? 'outgoing' : '' }}
                                  address="@if(!is_null(old('address'))){{old('address')}}@elseif(!is_null($cv)){{$cv->address}}@endif"
                    ></place-select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>@lang('cv.education')</label>
            <div>
                @foreach (__('options.education') as $key => $value)
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio"
                               id="education_{{ $key }}"
                               name="education"
                               value="{{ $key }}"
                               class="custom-control-input"
                               @if (old('education') === (string)$key || (!is_null($cv) && $cv->education === (string)$key) || $loop->index === 0) checked @endif
                        >
                        <label class="custom-control-label" for="education_{{ $key }}">{{ $value }}</label>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="form-group">
            <div data-provider="repeater" data-setlist="@if(is_array(old('education_details'))){{json_encode(old('education_details'))}}@elseif(!is_null($cv) && is_array($cv->education_details)){{json_encode($cv->education_details)}}@endif">
                <div data-repeater-list="education_details">
                    <div class="border rounded mb-3 px-2 py-3" data-repeater-item>
                        <div class="form-group">
                            <label>@lang('cv.institution')</label>
                            <input type="text" class="form-control" name="institution">
                        </div>
                        <div class="form-group">
                            <label>@lang('cv.profession')</label>
                            <input type="text" class="form-control" name="profession">
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <label>@lang('cv.start_date')</label>
                                <input type="text" name="start_date" class="form-control" data-provide="yearpicker" autocomplete="off">
                            </div>
                            <div class="col-4">
                                <label>@lang('cv.end_date')</label>
                                <input type="text" name="end_date" class="form-control" data-provide="yearpicker" autocomplete="off">
                            </div>
                            <div class="col-4 text-right">
                                <label class="d-block invisible">e</label>
                                <button type="button" data-repeater-delete class="btn btn-outline-danger">
                                    <i class="fa fa-close"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <button type="button" data-repeater-create class="btn btn-outline-primary">
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>@lang('cv.languages')</label>
            <div>
                @foreach(__('options.languages') as $key => $lang)
                    <div class="custom-control custom-checkbox custom-control-inline">
                        <input type="checkbox"
                               class="custom-control-input"
                               name="languages[]"
                               id="languge_{{ $key }}"
                               value="{{ $key }}"
                               @if ((is_array(old('languages')) && in_array($key, old('languages'))) || (!is_null($cv) && in_array($key, $cv->languages)) || $loop->index === 0)
                               checked
                                @endif
                        >
                        <label class="custom-control-label" for="languge_{{ $key }}">{{ $lang }}</label>
                    </div>
                @endforeach
            </div>
            <span class="form-text text-danger">{{ $errors->has('languages') ? __('cv.languageRequired') : '' }}</span>
        </div>
        <div class="form-group">
            <label>@lang('cv.desired_sphere')</label>
            <div data-provider="repeater" data-setlist="@if(old('spheres')){{json_encode(old('spheres'))}}@elseif(!is_null($spheresSetList)){{json_encode($spheresSetList)}}@endif">
                <div data-repeater-list="spheres">
                    <div class="border rounded mb-3 px-2 py-3" data-repeater-item>
                        <div class="row">
                            <div class="col-md-7">
                                <div class="form-group">
                                    <label>@lang('cv.sphere')</label>
                                    <select name="sphere" class="form-control">
                                        @foreach($spheres as $sphere)
                                            <option value="{{ $sphere->id }}">{{ $sphere->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>@lang('cv.sphere_experience')</label>
                                    <select name="experience" class="form-control">
                                        @foreach(__('options.experiences') as $key => $experience)
                                            <option value="{{ $key }}">{{ $experience }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2 text-right">
                                <label class="d-none d-md-block invisible">e</label>
                                <button type="button" data-repeater-delete class="btn btn-outline-danger">
                                    <i class="fa fa-close"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <button type="button" data-repeater-create class="btn btn-outline-primary">
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>@lang('cv.experience')</label>
            <div data-provider="repeater" data-setlist="@if(old('experience')){{json_encode(old('experience'))}}@elseif(!is_null($cv) && !is_null($cv->experiences)){{json_encode($cv->experiences)}}@endif">
                <div class="row" data-repeater-list="experience">
                    <div class="col-md-6" data-repeater-item>
                        <div class="border rounded mb-2 px-2 py-3">
                            <div class="form-group">
                                <label>@lang('cv.organization')</label>
                                <input type="text" class="form-control" name="organization">
                            </div>
                            <div class="form-group">
                                <label>@lang('cv.sphere')</label>
                                <select name="sphere_id" class="form-control">
                                    @foreach($spheres as $sphere)
                                        <option value="{{ $sphere->id }}">{{ $sphere->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>@lang('cv.position')</label>
                                <input type="text" class="form-control" name="position">
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>@lang('cv.start_date')</label>
                                        <input type="text" name="start_date" class="form-control" data-provide="monthyearpicker" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>@lang('cv.end_date')</label>
                                        <input type="text" name="end_date" class="form-control" data-provide="monthyearpicker" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-4 text-right">
                                    <label class="d-block invisible">e</label>
                                    <button type="button" data-repeater-delete class="btn btn-outline-danger">
                                        <i class="fa fa-close"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <button type="button" data-repeater-create class="btn btn-outline-primary">
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="skills">@lang('cv.skills')</label>
            <textarea name="skills" id="skills" class="autoheight" data-provider="tinymce">@if(old('skills')){{old('skills')}}@elseif(!is_null($cv)){{$cv->skills}}@endif</textarea>
        </div>
        <div class="form-group">
            <label for="military_book">@lang('cv.military_book')</label>
            <div class="custom-control custom-switch">
                <input
                        type="checkbox"
                        class="custom-control-input"
                        id="military_book"
                        name="military_book"
                        {{ !is_null(old('military_book')) || (!is_null($cv) && $cv->military_book) ? 'checked' : '' }}
                >
                <label class="custom-control-label" for="military_book">@lang('cv.available')</label>
            </div>
        </div>
        <div class="form-group">
            <label for="drive_right">@lang('cv.drive_right')</label>
            <div class="custom-control custom-switch">
                <input
                        type="checkbox"
                        class="custom-control-input"
                        id="drive_right"
                        name="drive_right"
                        {{ !is_null(old('drive_right')) || (!is_null($cv) && $cv->drive_right) ? 'checked' : '' }}
                >
                <label class="custom-control-label" for="drive_right">@lang('cv.available')</label>
            </div>
        </div>
        <div class="text-right">
            <p>@lang('cv.required_warning')</p>
            <button type="submit" class="btn btn-primary">
                <i class="fa fa-check"></i>
            </button>
        </div>
    </form>
</div>
@endsection
@push('scripts')
    <script src="{{ asset('theme/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('theme/vendor/bootstrap-datepicker/bootstrap-datepicker.'. App::getLocale() .'.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.init.js') }}"></script>
    <script src="{{ asset('theme/vendor/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('js/tinymce.init.js') }}"></script>
    <script src="{{ asset('js/jquery.repeater.init.js') }}"></script>
@endpush