@extends('layouts.client')
@section('content')
    <div class="container">
        @include('pdf.cv', ['cv' => $cv, 'user' => $cv->user])
    </div>
@endsection