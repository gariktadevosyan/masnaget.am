@extends('layouts.client')
@section('content')
<section class="hero">
    <div class="container mb-5">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <h1 class="hero-heading mb-0">Move work <br> forward</h1>
                <div class="row">
                    <div class="col-lg-10">
                        <p class="lead text-muted mt-4 mb-4">Lorem ipsum dolor sit amet, consectetur. Eiusmod tempor incididunt.</p>
                    </div>
                </div>
                <form action="/search" method="get">
                    <div class="input-group">
                        <input type="text" name="kwd" class="form-control" placeholder="Բանալի բառեր․․․">
                        <div class="input-group-append">
                            <button class="btn btn-outline-primary" type="submit">Փնտրել</button>
                        </div>
                    </div>
                    <a href="/search">ընդլայնված որոնում</a>
                    @if($userRole === 'employer')
                        <a href="/search/cv" class="ml-2">փնտրել մասնգետներ</a>
                    @endif
                </form>
            </div>
            <div class="col-lg-6">
                <img src="https://via.placeholder.com/540x425.png?text=Masnaget.am" alt="..." class="hero-image img-fluid d-none d-lg-block">
            </div>
        </div>
    </div>
</section>

<section class="bg-primary text-white py-4">
    <div class="container">
        <div class="text-center">
            @foreach($employers as $employer)
                <a href="/employer/{{ $employer->id }}">
                    <div class="card d-inline-block rounded-circle" style="width: 180px; height: 180px">
                        <div class="card-body">
                            <img src="{{ $employer->logo }}">
                            <p>{{ $employer->name }}</p>
                        </div>
                    </div>
                </a>
            @endforeach
        </div>
    </div>
</section>

<section class="container">
    <job-listing></job-listing>
</section>
@endsection