<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">

    <title>{{ config('app.name') }}</title>

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('front/vendor/font-awesome/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/font-face.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('front/css/fontastic.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/style.sea.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/solid.css" integrity="sha384-QokYePQSOwpBDuhlHOsX0ymF6R/vLk/UQVz3WHa6wygxI5oGTmDTv8wahFOSspdm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/fontawesome.css" integrity="sha384-vd1e11sR28tEK9YANUtpIOdjGW14pS87bUBuOIoBILVWLFnS+MCX9T6MMf0VdPGq" crossorigin="anonymous">
    <link rel="shortcut icon" href="img/favicon.png">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @stack('styles')
</head>
<body class="client">
    <!-- navbar-->

    <main role="main" id="app">
        <header class="header">
            <nav class="navbar navbar-expand-lg fixed-top">
                <div class="container">
                    <a href="{{ config('app.url') }}" class="navbar-brand">
                        <img src="https://via.placeholder.com/120x60.png?text={{ config('app.name') }}" alt="{{ config('app.name') }}" class="img-fluid">
                    </a>
                    <button type="button"
                            data-toggle="collapse"
                            data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent"
                            aria-expanded="false"
                            aria-label="Toggle navigation"
                            class="navbar-toggler navbar-toggler-right"
                    >
                        <i class="fa fa-bars ml-2"></i>
                    </button>
                    <div id="navbarSupportedContent" class="collapse navbar-collapse">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a href="/" class="nav-link {{ Route::current()->uri === "/" ? 'active' : '' }}">@lang('client.home')</a>
                            </li>
                            <li class="nav-item dropdown">
                                @include('includes.locale_dropdown', ['navLink' => true])
                            </li>
                            @auth
                            <li class="nav-item">
                                @if($user->isRole('employee'))
                                    <a href="{{ route('cv.index') }}" class="nav-link">@lang('client.my_cv')</a>
                                @endif

                                @if($user->isRole('employer'))
                                    <a href="{{ route('admin.index') }}" class="nav-link">@lang('client.administration')</a>
                                @endif
                            </li>
                            @endauth
                            @auth
                                @include('includes.profile_dropdown')
                            @endauth
                            @guest
                                <li class="nav-item">
                                    <a href="#" data-toggle="modal" data-target="#loginModal" class="nav-link">@lang('auth.login')</a>
                                </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <section class="hero">
            @yield('content')
        </section>
    </main>
    <footer class="main-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 mb-5 mb-lg-0">
                    <div class="footer-logo">
                        <img src="https://via.placeholder.com/120x60.png?text={{ config('app.name') }}" alt="{{ config('app.name') }}" class="img-fluid">
                    </div>
                </div>
                <div class="col-lg-3 mb-5 mb-lg-0">
                    <h5 class="footer-heading">Site pages</h5>
                    <ul class="list-unstyled">
                        <li>
                            <a href="{{ config('app.url') }}" class="footer-link">Home</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 mb-5 mb-lg-0">
                    <h5 class="footer-heading">Product</h5>
                    <ul class="list-unstyled">
                        <li> <a href="#" class="footer-link">Why Appton?</a></li>
                    </ul>
                </div>
                <div class="col-lg-3">
                    <h5 class="footer-heading">Resources</h5>
                    <ul class="list-unstyled">
                        <li> <a href="#" class="footer-link">Download</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="copyrights">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 text-center text-lg-left">
                        <p class="copyrights-text mb-3 mb-lg-0">&copy; All rights reserved.</p>

                    </div>
                    <div class="col-lg-6 text-center text-lg-right">
                        <ul class="list-inline social mb-0">
                            <li class="list-inline-item"><a href="#" class="social-link"><i class="fa fa-facebook"></i></a><a href="#" class="social-link"><i class="fa fa-twitter"></i></a><a href="#" class="social-link"><i class="fa fa-youtube-play"></i></a><a href="#" class="social-link"><i class="fa fa-vimeo"></i></a><a href="#" class="social-link"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Login Modal-->
    <div id="loginModal" tabindex="-1" role="dialog" aria-hidden="false" class="modal fade bd-example-modal-lg">
        <div role="document" class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header border-bottom-0">
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body p-4 p-lg-5">
                    <form action="{{ route('login') }}" method="post" class="login-form text-left" novalidate>
                        @csrf
                        <div class="form-group mb-2">
                            <label>@lang('auth.email')</label>
                            <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group mb-2">
                            <label>@lang('auth.password')</label>
                            <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group row">
                            <div class="col">
                                <div class="login-checkbox">
                                    <div class="custom-control custom-checkbox">
                                        <input
                                                type="checkbox"
                                                class="custom-control-input"
                                                name="remember"
                                                id="remember"
                                                checked
                                        >
                                        <label class="custom-control-label" for="remember">@lang('auth.stay_logged_in')</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col text-right">
                                <a href="{{ route('password.request') }}"><small>@lang('auth.forgot_password')</small></a>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">@lang('auth.login')</button>
                        </div>
                        <div class="form-group text-center">
                            <a href="{{ route('register') }}">@lang('auth.register')</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @if (!is_null($user) && is_null($user->email_verified_at))
        <div id="registeredModal" tabindex="-1" role="dialog" aria-hidden="false" class="modal fade bd-example-modal-lg">
            <div role="document" class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header border-bottom-0">
                        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body p-4 p-lg-5">
                        <p>@lang('auth.registered', ['email' => $user->email])</p>
                        <div class="text-right">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">@lang('client.close')</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

<!-- JavaScript files-->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('front/js/front.js') }}"></script>
@stack('scripts')
@if($errors->has('email') || $errors->has('password'))
    <script>
        $('#loginModal').modal('show');
    </script>
@endif
@if(!is_null($user) && is_null($user->email_verified_at))
    <script>
        $('#registeredModal').modal('show');
    </script>
@endif
@if (session('saved'))
    <script>
        Toastr.success('{{ __('client.saved') }}');
    </script>
@endif
@if (session('applicationSent'))
    <script>
        Toastr.success('{{ __('client.applicationSuccess') }}');
    </script>
@endif
</body>
</html>