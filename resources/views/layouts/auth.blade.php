<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Garegin Tadevosyan">
    <meta name="robots" content="noindex">

    <title>{{ config('app.name') }} - @yield('title')</title>

    <link href="{{ asset('theme/css/font-face.css') }}" rel="stylesheet" media="all">
    <link rel="stylesheet" href="{{ asset('front/vendor/font-awesome/css/font-awesome.css') }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('theme/css/theme.css') }}" rel="stylesheet" media="all">
</head>
<body>
<main role="main" class="page-wrapper" id="app">
    <div class="page-content--bge5">
        <div class="container">
            <div class="login-wrap">
                <div class="dropdown text-right">
                    @include('includes.locale_dropdown', ['navLink' => false])
                </div>
                @yield('content')
            </div>
        </div>
    </div>
</main>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
