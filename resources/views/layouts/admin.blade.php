<!DOCTYPE html>
<html lang="hy">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Garegin Tadevosyan">
    <meta name="robots" content="noindex">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') . ' - Կառավարում' }}</title>

    <link rel="stylesheet" href="{{ asset('theme/css/font-face.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('front/vendor/font-awesome/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/vendor/mdi-font/css/material-design-iconic-font.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('theme/vendor/css-hamburgers/hamburgers.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('theme/css/theme.css') }}" media="all">
    @stack('styles')
</head>

<body>
<div class="page-wrapper">
    <header class="header-mobile d-block d-lg-none bg-white">
        <div class="header-mobile__bar">
            <div class="container-fluid">
                <div class="header-mobile-inner">
                    <a class="logo" href="{{ config('app.url') }}">
                        <img src="https://via.placeholder.com/500x59.png?text={{ config('app.name') }}" alt="{{ config('app.name') }}">
                    </a>
                    <button class="hamburger hamburger--slider" type="button">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
        </div>
        <nav class="navbar-mobile">
            <div class="container-fluid">
                <ul class="navbar-mobile__list list-unstyled">
                    @include('layouts.menu_items')
                </ul>
            </div>
        </nav>
    </header>
    <aside class="menu-sidebar d-none d-lg-block">
        <div class="logo">
            <a href="{{ config('app.url') }}">
                <img src="https://via.placeholder.com/300x75.png?text={{ config('app.name') }}" alt="{{ config('app.name') }}">
            </a>
        </div>
        <div class="menu-sidebar__content js-scrollbar1">
            <nav class="navbar-sidebar">
                <ul class="list-unstyled navbar__list">
                    @include('layouts.menu_items')
                </ul>
            </nav>
        </div>
    </aside>
    <div class="page-container">
        <header class="header-desktop">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="header-wrap">
                        <form class="form-header" action="" method="POST">
                            <input class="au-input au-input--xl" type="text" name="search"
                                   placeholder="Search for datas &amp; reports..."/>
                            <button class="au-btn--submit" type="submit">
                                <i class="zmdi zmdi-search"></i>
                            </button>
                        </form>
                        <div class="header-button">
                            <div class="noti-wrap">

                            </div>
                            <div class="account-wrap">
                                <div class="account-item clearfix js-item-menu">
                                    <div class="image">
                                        <img src="{{ $userDetails->logo }}" alt="logo"/>
                                    </div>
                                    <div class="content">
                                        <a class="js-acc-btn" href="#">{{ $userDetails->name }}</a>
                                    </div>
                                    <div class="account-dropdown js-dropdown">
                                        <div class="info clearfix">
                                            <div class="image">
                                                <a href="#">
                                                    <img src="{{ $userDetails->logo }}" alt="logo"/>
                                                </a>
                                            </div>
                                            <div class="content">
                                                <h5 class="name">
                                                    <a href="#">{{ $userDetails->name }}</a>
                                                </h5>
                                                <span class="email">{{ $userDetails->email }}</span>
                                            </div>
                                        </div>
                                        {{--<div class="account-dropdown__body">--}}
                                            {{--<div class="account-dropdown__item">--}}
                                                {{--<a href="#">--}}
                                                    {{--<i class="zmdi zmdi-account"></i>Account</a>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        <div class="account-dropdown__footer">
                                            <a href="#" onclick="logout_form.submit()">
                                                <i class="zmdi zmdi-power"></i>Ելք
                                            </a>
                                            <form action="{{ route('logout') }}" method="post" id="logout_form">@csrf</form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <main role="main" id="app" class="main-content">
            <div class="section__content">
                @yield('content')
            </div>
        </main>
    </div>
</div>

<script src="{{ asset('js/app.js') }}"></script>
@stack('scripts')
<script src="{{ asset('theme/js/main.js') }}"></script>
@if (session('saved'))
    <script>
        Toastr.success('{{ __('client.saved') }}');
    </script>
@endif
</body>

</html>