<li class="{{ Route::currentRouteName() === 'admin.index' ? 'active' : '' }}">
    <a href="{{ route('admin.index') }}">
        - Գլխավոր
    </a>
</li>
@if ($user->isRole('employer'))
    <li class="{{ Route::currentRouteName() === 'job-offers.index' ? 'active' : '' }}">
        <a href="{{ route('job-offers.index') }}">
            - Հայտարարություններ
        </a>
    </li>
    <li class="{{ Route::currentRouteName() === 'settings.index' ? 'active' : '' }}">
        <a href="{{ route('settings.index') }}">
            <i class="fa fa-cog"></i> Կարգավորումներ
        </a>
    </li>
@endif
