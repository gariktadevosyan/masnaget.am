@extends("layouts.auth")
@section("title", __('auth.register'))
@section("content")
<div class="login-content">
    <div class="login-logo">
        <a href="{{ config('app.url') }}">
            <img src="https://via.placeholder.com/480x59.png?text={{ config('app.name') }}" alt="{{ config('app.name') }}">
        </a>
    </div>
    <div class="login-form">
        <form action="/register" method="post" novalidate>
            @csrf
            <div class="form-group">
                <label for="email">@lang('auth.email')</label>
                <input
                    id="email"
                    class="au-input au-input--full form-control @if($errors->has('email')){{'is-invalid'}}@endif"
                    type="email"
                    name="email"
                    value="{{ old('email') }}"
                >
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="password">@lang('auth.password')</label>
                <input
                    id="password"
                    class="au-input au-input--full form-control @if($errors->has('password')){{'is-invalid'}}@endif"
                    type="password"
                    name="password"
                >
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="password_confirm">@lang('auth.password_confirmation')</label>
                <input
                    id="password_confirm"
                    class="au-input au-input--full form-control"
                    type="password"
                    name="password_confirmation"
                >
            </div>
            <div class="form-group">
                <label for="role">@lang('auth.register_as')</label>
                <select name="role" id="role" class="au-input au-input--full form-control" @if($errors->has('role')){{'is-invalid'}}@endif>
                    @foreach($roles AS $role)
                        <option value="{{ $role->id }}">@lang('roles.' . $role->title)</option>
                    @endforeach
                </select>
                @if ($errors->has('role'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('role') }}</strong>
                    </span>
                @endif
            </div>
            <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">@lang('auth.register_submit')</button>
        </form>
        <div class="register-link">
            <p>
                <a href="{{ route('login') }}">@lang('auth.already_registered')</a>
            </p>
        </div>
    </div>
</div>
@endsection