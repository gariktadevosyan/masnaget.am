@extends('layouts.auth')
@section('title', __('auth.verify'))
@section('content')
<div class="login-content">
    <div class="card-body">
        <h4 class="card-title mb-3">@lang('auth.verify')</h4>
        <p class="card-text">
            @if (session('resent'))
                <p>
                    @lang('auth.verify_email')
                </p>
            @endif

            @lang('auth.check_inbox')
            <a href="{{ route('verification.resend') }}">@lang('auth.reset')</a>
        </p>
        <div class="text-right">
            <a href="/">
                <i class="fa fa-long-arrow-left mr-2"></i>
                @lang('auth.back')
            </a>
        </div>
    </div>
</div>
@endsection