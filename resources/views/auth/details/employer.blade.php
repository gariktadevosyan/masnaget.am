<!DOCTYPE html>
<html lang="hy">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Garegin Tadevosyan">
    <meta name="robots" content="noindex">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <link rel="stylesheet" href="{{ asset('theme/css/font-face.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('front/vendor/font-awesome/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('css/bs-stepper.css') }}" media="all">
</head>

<body>
<main role="main" id="app">
    <div class="container">
        <div class="py-3">
            <div class="card">
                <div class="card-body">
                    <div data-provider="stepper" class="bs-stepper">
                        <div class="bs-stepper-header">
                            <div class="step" data-target="#general_step">
                                <a href="#">
                                    <span class="bs-stepper-circle">1</span>
                                    <span class="bs-stepper-label">Ընհանուր</span>
                                </a>
                            </div>
                            <div class="line"></div>
                            <div class="step" data-target="#address_step">
                                <a href="#">
                                    <span class="bs-stepper-circle">2</span>
                                    <span class="bs-stepper-label">Հասցե</span>
                                </a>
                            </div>
                            <div class="line"></div>
                            <div class="step" data-target="#contacts_step">
                                <a href="#">
                                    <span class="bs-stepper-circle">3</span>
                                    <span class="bs-stepper-label">Կոնտակտներ</span>
                                </a>
                            </div>
                            <div class="line"></div>
                            <div class="step" data-target="#about_us_step">
                                <a href="#">
                                    <span class="bs-stepper-circle">3</span>
                                    <span class="bs-stepper-label">Կազմակերպության մասին</span>
                                </a>
                            </div>
                        </div>
                        <div class="bs-stepper-content">
                            <form action="{{ route('settings.store') }}" method="post">
                                @csrf
                                @method('POST')
                                <div id="general_step" class="content">
                                    <div class="form-group">
                                        <img-upload name="logo" required old="{{old('logo')}}" title="Բեռնել նկար"></img-upload>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="required">Անվանում</label>
                                        <input type="text" id="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" maxlength="50" value="{{ old('name') }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="type">Տեսակ</label>
                                        <select id="type" class="form-control" name="type">
                                            <option value="ԱՁ">ԱՁ/Անհատ ձեռնարկատեր</option>
                                            <option value="ՍՊԸ">ՍՊԸ/Սահմանափակ պատասխանատվությամբ ընկերություն</option>
                                            <option value="ՓԲԸ">ՓԲԸ/Փակ բաժնետիրական ընկերություն</option>
                                            <option value="ԲԲԸ">ԲԲԸ/Բաց  բաժնետիրական ընկերություն</option>
                                            <option value="ԼԻ">ԼԻ/Լիակատար ընկերակցություն</option>
                                            <option value="ՎՎՀԸ">ՎՎՀԸ/Վստահության վրա հիմնված ընկերություն</option>
                                            <option value="ԼՊԸ">ԼՊԸ/Լրացուցիչ պատասխանատվությամբ ընկերություն</option>
                                            <option value="Կ">Կոոպերատիվ</option>
                                            <option value="ԱԿ">ԱԿ/Արտադրական կոոպերատիվ</option>
                                            <option value="ՍԿ">ՍԿ/Սպառողական կոոպերատիվ</option>
                                            <option value="ՊՈԱԿ">ՊՈԱԿ/Պետական ոչ առևտրային կազմակերպություն</option>
                                            <option value="ՀՄԴ">ՀՄԴ/Հիմնադրամ</option>
                                            <option value="ՀՈԱԿ">ՀՈԱԿ/Համայնքային ոչ առևտրային կազմակերպություն</option>
                                            <option value="ԱՍ">ԱՍ/Առանձնացված ստորաբաժանում (ոչ առևտրային)</option>
                                            <option value="ՀԿ">ՀԿ/Հասարակական կազմակերպություն</option>
                                            <option value="ԻԱՄ">ԻԱՄ/Իրավաբանական անձանց միություն</option>
                                            <option value="ԱՐՀԿ">ԱՐՀԿ/Արհեստակցական կազմակերպություն</option>
                                            <option value="ՀՄՏ">ՀՄՏ/Համատիրություն</option>
                                            <option value="ՀՄ">ՀՄ/Հիմնարկ</option>
                                            <option value="ԴՁ">ԴՁ/Դուստր ձեռնարկություն</option>
                                            <option value="ՊՁ">ՊՁ/Պետական (իշխանական տեղական մարմնի) ձեռնարկություն</option>
                                            <option value="ԳԿՏ">ԳԿՏ/Գյուղացիական կոլեկտիվ տնտեսություն</option>
                                            <option value="ԿՍ">ԿՍ/Կուսակցություն</option>
                                            <option value="ՋԸ">ՋԸ/Ջրօգտագործողների ընկերություն</option>
                                            <option value="ԱԱՊ">ԱԱՊ/Առևտրա-արդյունաբերական պալատ</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="activity_sphere">Գործունեության ոլորտը</label>
                                        <select name="activity_sphere_id" id="activity_sphere" class="form-control">
                                            @foreach($activitySpheres as $sphere)
                                            <option value="{{ $sphere->id }}" {{ old('activity_sphere') == $sphere->id ? 'selected' : '' }}>{{ $sphere->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="tin" class="required">ՀՎՀՀ</label>
                                        <input type="text" id="tin" class="form-control {{ $errors->has('tin') ? 'is-invalid' : '' }}" name="tin" minlength="8" maxlength="8" placeholder="00000000" data-inputmask="'mask': '99999999'" value="{{ old('tin') }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="employees_count">Աշխատակիցների քանակը</label>
                                        <input type="number" id="employees_count" class="form-control" name="employees_count" min="0" value="{{ old('employees_count') }}">
                                    </div>
                                    <div class="text-right">
                                        <button type="button" class="btn btn-primary" onclick="myStepper.next()">Հաջորդը</button>
                                    </div>
                                </div>
                                <div id="address_step" class="content">
                                    <place-select old="{{old('community_id')}}" address="{{old('address')}}"></place-select>
                                    <div class="text-right">
                                        <button type="button" class="btn btn-primary" onclick="myStepper.previous()">Նախորդը</button>
                                        <button type="button" class="btn btn-primary" onclick="myStepper.next()">Հաջորդը</button>
                                    </div>
                                </div>
                                <div id="contacts_step" class="content">
                                    <div class="form-group">
                                        <label>Հեռախոս</label>
                                        <div data-provider="repeater" data-setlist="{{ old('phone') ? json_encode(old('phone')) : '' }}">
                                            <div data-repeater-list="phone">
                                                <div data-repeater-item>
                                                    <div class="input-group mb-3">
                                                        <input type="text" class="form-control" name="phone" placeholder="+374" value="+374">
                                                        <div class="input-group-append">
                                                            <button type="button" class="btn btn-danger" data-repeater-delete>
                                                                <i class="fa fa-minus"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-right">
                                                <button type="button" class="btn btn-primary" data-repeater-create>
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="required">Էլ․ փոստ</label>
                                        <input type="email" id="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" name="email" value="{{ old('email') ?: $user->email }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="website">Կայք</label>
                                        <input type="text" id="website" class="form-control" name="website" placeholder="http://" value="{{ old('website') ?: 'http://' }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="agent_name">Ներկայացուցիչ</label>
                                        <input type="text" id="agent_name" class="form-control" name="agent" placeholder="Անուն Ազգանուն" value="{{ old('agent') }}">
                                    </div>
                                    <div class="text-right">
                                        <button type="button" class="btn btn-primary" onclick="myStepper.previous()">Նախորդը</button>
                                        <button type="button" class="btn btn-primary" onclick="myStepper.next()">Հաջորդը</button>
                                    </div>
                                </div>
                                <div id="about_us_step" class="content">
                                    <div class="form-group">
                                        <textarea name="about" data-provider="tinymce" class="fullpage">{{old('about')}}</textarea>
                                    </div>
                                    <div class="text-right">
                                        <button type="button" class="btn btn-primary" onclick="myStepper.previous()">Նախորդը</button>
                                        <button type="submit" class="btn btn-success">Հաստատել</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <p class="text-right">@lang('cv.required_warning')</p>
                </div>
            </div>
        </div>
    </div>
</main>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('theme/vendor/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('js/tinymce.init.js') }}"></script>
<script src="{{ asset('js/stepper.init.js') }}"></script>
<script src="{{ asset('js/jquery.repeater.init.js') }}"></script>
</body>

</html>