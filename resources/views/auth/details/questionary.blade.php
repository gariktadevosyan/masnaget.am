<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Garegin Tadevosyan">
        <meta name="robots" content="noindex">

        <title>{{ config('app.name') }}</title>

        <link href="{{ asset('theme/css/font-face.css') }}" rel="stylesheet" media="all">
        <link rel="stylesheet" href="{{ asset('front/vendor/font-awesome/css/font-awesome.css') }}">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" media="all">
        <link rel="stylesheet" href="{{ asset('theme/vendor/bootstrap-datepicker/bootstrap-datepicker3.min.css') }}">
    </head>
    <body>
        <main role="main" class="py-4" id="app">
            <div class="container">
                <header>
                    <ul class="list-inline text-center">
                        <li class="list-inline-item">
                            <img src="{{ asset('img/b.png') }}" height="40">
                        </li>
                        <li class="list-inline-item">
                            <img src="{{ asset('img/bp.png') }}" height="40">
                        </li>
                        <li class="list-inline-item">
                            <img src="{{ asset('img/vd.png') }}" height="60">
                        </li>
                    </ul>

                    <h4 class="text-center"><strong>Մասնագիտական աշխատակազմի կարիքների ուսումնասիրություն կազմակերպություններում
                        Հարցում</strong>
                    </h4>
                    <p>
                        <p><b>Հարգելի գործընկեր,</b></p>

                        <p>Շնորհակալություն ենք հայտնում սույն հարցմանը մասնակցելու Ձեր համաձայնության համար: Հարցումն անց է կացվում  գերմանական <b>«Միջին մասնագիտական կրթության ուսուցում (ՄԿՈւ) համագործակցության ծրագրի» շրջանակներում, որը նպատակաուղղված է երիտասարդների զբաղվածության խթանմանը:</b>
                        Ծրագրի գործընկերներն են՝ պետական կառույցներ (ՀՀ կրթության և գիտության, աշխատանքի և սոցիալական հարցերի, տնտեսական զարգացման և ներդրումների նախարարություններ), դպրոցներ, քոլեջներ և փոքր ու միջին բիզնեսի ներկայացուցիչներ:</p>

                        <p>Հարցման նպատակն է Հայաստանի տնտեսության 4 գերակա ոլորտներում՝ տեղեկատվական տեխնոլոգիաներ և ինժիներինգ, սննդի արդյունաբերություն, տուրիզմ և ծառայությունների մատուցում, բացահայտել գործատուների պահանջարկը մասնագիտական կադրերի նկատմամբ: Այս հարցաթերթիկով մենք փորձում ենք ստանալ  վերոնշյալ ոլորտներում գործող ընկերություններում առկա զբաղվածության կառուցվածքի, կադրերի հոսունության, ինչպես նաև ռեսուրսների կարիքի քանակական պատկերը, որպեսզի կարողանանք Ձեր ներկայացրած որակական տեղեկությունների հետ միասին ավելի խորը պատկերացում կազմել աշխատուժի պակասի այդքան շատ քննարկված խնդրահարույց թեմայի մասին և անցկացնել նրա նախնական կարիքների գնահատումը:</p>

                        <p>Բոլոր տվյալները պահվելու են գաղտնի: Ոչ մի տվյալ, որը կբացահայտի Ձեր մասնակցությունը կամ կպարունակի Ձեր կազմակերպության մասին տեղեկություններ, չի հրապարակվելու:</p>

                    </p>
                </header>
                <form action="/">
                    <div class="form-group row">
                        <label for="name" class="col-lg-6 col-form-label">Հարցվող  կազմակերպության անվանում</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" id="name">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-4">
                            <div class="row">
                                <label for="contact_person" class="col-lg-4 col-form-label">Կոնտակտային անձ</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" id="contact_person">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="row">
                                <label for="contact_phone" class="col-lg-4 col-form-label">հեռախոս</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" id="contact_phone">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="row">
                                <label for="contact_email" class="col-lg-4 col-form-label">Էլ. հասցե</label>
                                <div class="col-lg-8">
                                    <input type="email" class="form-control" id="contact_email">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label for="sphere">Գործունեության ոլորտը</label>
                            <select id="sphere" class="form-control">
                                @foreach($spheres as $sphere)
                                    <option value="{{ $sphere->id }}">{{ $sphere->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-6">
                            <label>Գործունեության վայրը</label>
                            <place-select old=""></place-select>
                        </div>
                    </div>

                    <h5 class=""><strong>ՄԱՍ 1. ԶԲԱՂՎԱԾՈՒԹՅԱՆ ՆԵՐԿԱՅԻՍ ԿԱՌՈՒՑՎԱԾՔԻ ՊԱՏԿԵՐԸ ԿԱԶՄԱԿԵՐՊՈՒԹՅՈՒՆՈՒՄ</strong></h5>
                    <div class="row">
                        <div class="col"><b>1.	Զբաղվածների քանակն ու կառուցվածքը</b></div>
                        <div class="col"><input type="text" class="form-control" data-provide="datepicker"></div>
                        <div class="col"><b>դրությամբ</b></div>
                    </div>
                    <div class="form-group">
                        <label><b>1.1. Միջնակարգ կրթությամբ աշխատակիցներ</b></label>
                        <p>Տ-տղամարդիկ, Կ-կանայք, տ-տարեկան</p>
                        <table border="1" cellpadding="5" width="100%">
                            <tr>
                                <th>Տարիք</th>
                                <td colspan="2">15ից մինչև 25 տ </td>
                                <td colspan="2">25ից մինչև 35տ</td>
                                <td colspan="2">35ից մինչև 63տ</td>
                                <td colspan="2">63ից բարձր</td>
                                <td colspan="2">Ընդամենը</td>
                            </tr>
                            <tr>
                                <th>Սեռ</th>
                                <td>Տ</td>
                                <td>Կ</td>
                                <td>Տ</td>
                                <td>Կ</td>
                                <td>Տ</td>
                                <td>Կ</td>
                                <td>Տ</td>
                                <td>Կ</td>
                                <td>Տ</td>
                                <td>Կ</td>
                            </tr>
                            <tr>
                                <th>Քանակ</th>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="form-group">
                        <label><b>1.2.Միջին մասնագիտական կրթությամբ, ներառյալ`  նախնական  (արհեստագործական),  աշխատակիցներ</b></label>
                        <table border="1" cellpadding="5" width="100%">
                            <tr>
                                <td colspan="2">18ից մինչև 25 տ </td>
                                <td colspan="2">25ից մինչև 35տ</td>
                                <td colspan="2">35ից մինչև 63տ</td>
                                <td colspan="2">63ից բարձր</td>
                                <td colspan="2">Ընդամենը</td>
                            </tr>
                            <tr>
                                <td>Տ</td>
                                <td>Կ</td>
                                <td>Տ</td>
                                <td>Կ</td>
                                <td>Տ</td>
                                <td>Կ</td>
                                <td>Տ</td>
                                <td>Կ</td>
                                <td>Տ</td>
                                <td>Կ</td>
                            </tr>
                            <tr>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="form-group">
                        <label><b>1.3.Բարձրագույն կրթությամբ աշխատակիցներ</b></label>
                        <table border="1" cellpadding="5" width="100%">
                            <tr>
                                <td colspan="2">18ից մինչև 25 տ </td>
                                <td colspan="2">25ից մինչև 35տ</td>
                                <td colspan="2">35ից մինչև 63տ</td>
                                <td colspan="2">63ից բարձր</td>
                                <td colspan="2">Ընդամենը</td>
                            </tr>
                            <tr>
                                <td>Տ</td>
                                <td>Կ</td>
                                <td>Տ</td>
                                <td>Կ</td>
                                <td>Տ</td>
                                <td>Կ</td>
                                <td>Տ</td>
                                <td>Կ</td>
                                <td>Տ</td>
                                <td>Կ</td>
                            </tr>
                            <tr>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="form-group">
                        <label><b>1.4.Կա՞ն ուսանողներ Ձեր կազմակերպությունում</b></label>
                        <div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="q1-4-y" name="q1-4">
                                <label class="form-check-label" for="q1-4-y">Այո</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="q1-4" id="q1-4-n">
                                <label class="form-check-label" for="q1-4-n">Ոչ</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label><b>1.4.1 Եթե այո, ապա լրացրեք հետևյալ աղյուսակը</b></label>
                        <div class="row">
                            <div class="col-sm-6">
                                <b>Թիրախային մասնագիտություն </b>
                            </div>
                            <div class="col-sm-3">
                                <b>ուսանողների քանակ</b>
                            </div>
                        </div>
                        <div data-provider="repeater">
                            <div data-repeater-list="1-4-1">
                                <div data-repeater-item>
                                    <div class="form-group row">
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="number" class="form-control">
                                        </div>
                                        <div class="col-sm-3">
                                            <button type="button" class="btn btn-danger" data-repeater-delete>
                                                <i class="fa fa-minus"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 ml-auto">
                                    <button type="button" class="btn btn-primary" data-repeater-create>
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label><b>1.4.2 Եթե ոչ, ապա որո՞նք են 3 առավել հաճախ հանդիպող մասնագիտությունները կամ աշխատանքի տեսակները Ձեր ընկերությունում (ըստ աշխատակիցների քանակի՝ սկսելով ամենահաճախ պահանջվող մասնագիտությունից կամ աշխատանքից):</b></label>
                        <div class="row">
                            <div class="col-sm-6">
                                <b>Մասնագիտության անվանում / աշխատանքի տեսակը</b>
                            </div>
                            <div class="col-sm-3">
                                <b>Քանակ</b>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <input type="text" class="form-control">
                            </div>
                            <div class="col-sm-3">
                                <input type="number" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <input type="text" class="form-control">
                            </div>
                            <div class="col-sm-3">
                                <input type="number" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <input type="text" class="form-control">
                            </div>
                            <div class="col-sm-3">
                                <input type="number" class="form-control">
                            </div>
                        </div>
                    </div>

                    <h5 class=""><strong>ՄԱՍ  2. ԿԱԴՐԵՐԻ ՀՈՍՈՒՆՈՒԹՅՈՒՆՆ ԱՆՑԱԾ 12 ԱՄԻՍՆԵՐԻ ԸՆԹԱՑՔՈՒՄ</strong></h5>
                    <div class="form-group">
                        <label><b>2. Արդյո՞ք նշված ժամանակահատվածում Ձեր ընկերությունից հեռացե՞լ են աշխատակիցներ (01.01.2017-01.01.2018թ. ): Եթե այո, ապա որքա՞ն և ինչ պատճառով</b></label>
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="q2-2" id="q2-2-y">
                                    <label class="form-check-label" for="q2-2">
                                        Այո
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="q2-2" id="q2-2-n" checked>
                                    <label class="form-check-label" for="q2-2-n">
                                        Ոչ
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-9">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Պատճառ</th>
                                            <th>Քանակ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>ցածր աշխատավարձ</td>
                                            <td><input type="number" class="form-control"></td>
                                        </tr>
                                        <tr>
                                            <td>փոխում է մասնագիտությունը</td>
                                            <td><input type="number" class="form-control"></td>
                                        </tr>
                                        <tr>
                                            <td>մեկնում են այլ երկիր</td>
                                            <td><input type="number" class="form-control"></td>
                                        </tr>
                                        <tr>
                                            <td>աշխատանքային միգրացիա</td>
                                            <td><input type="number" class="form-control"></td>
                                        </tr>
                                        <tr>
                                            <td>աշխատանքային պայմաններ</td>
                                            <td><input type="number" class="form-control"></td>
                                        </tr>
                                        <tr>
                                            <td>այլ</td>
                                            <td><input type="number" class="form-control"></td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Ընդամենը</th>
                                            <td><input type="number" class="form-control"></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label><b>3. Վերջին 6 ամիսների ընթացքում արդյո՞ք ընդունե՞լ եք նոր աշխատակիցներ
                                (01.01.2017-01.01.2018թ.):Եթե այո, ապա որքա՞ն և ո՞ր աշխատանքների համար
                            </b></label>
                        <div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="q2-3" id="q2-3-y">
                                <label class="form-check-label" for="q2-3-y">Այո</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="q2-3" id="q2-3-n" checked>
                                <label class="form-check-label" for="q2-3-n">Ոչ</label>
                            </div>
                        </div>
                        <div class="mt-2">
                            <label><b>Եթե ոչ՝ անցնել հարց 7-ին</b></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <b>Եթե այո, ապա նշեք աշխատանքի տեսակը</b>
                            </div>
                            <div class="col-sm-3">
                                <b>քանակ</b>
                            </div>
                        </div>
                        <div data-provider="repeater">
                            <div data-repeater-list="2-2-3">
                                <div data-repeater-item>
                                    <div class="form-group row">
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="number" class="form-control">
                                        </div>
                                        <div class="col-sm-3">
                                            <button type="button" class="btn btn-danger" data-repeater-delete>
                                                <i class="fa fa-minus"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <b>ԸՆԴԱՄԵՆԸ</b>
                                </div>
                                <div class="col-sm-3">
                                    <input type="number" class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 ml-auto">
                                    <button type="button" class="btn btn-primary" data-repeater-create>
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label><b>4. Ո՞րն է նոր աշխատակիցների ընդունման առաջնային պատճառը</b></label>
                        <table class="table">
                            <tr>
                                <td>Փոխարինող աշխատուժի ապահովում</td>
                                <td><input type="radio" name="2-2-4"></td>
                            </tr>
                            <tr>
                                <td>Լրացուցիչ աշխատուժի ապահովում</td>
                                <td><input type="radio" name="2-2-4"></td>
                            </tr>
                            <tr>
                                <td>Աշխատանքի ծավալի ավելացում (տնտեսական առումով)</td>
                                <td><input type="radio" name="2-2-4"></td>
                            </tr>
                            <tr>
                                <td>Կարճաժամկետ (սեզոնային) աշխատանքների համար     </td>
                                <td><input type="radio" name="2-2-4"></td>
                            </tr>
                            <tr>
                                <td>Այլ</td>
                                <td><input type="radio" name="2-2-4"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="form-group">
                        <label><b>5. Արդյո՞ք ունեցել եք դժվարություններ թափուր աշխատատեղերը լրացնելու հարցում. անցած 6 ամիսների ընթացքում  (01.01.2017-01.01.2018թ. ):
                            </b></label>
                        <div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="q2-5" id="q2-5-y">
                                <label class="form-check-label" for="q2-5-y">Այո</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="q2-5" id="q2-5-n" checked>
                                <label class="form-check-label" for="q2-5-n">Ոչ</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label><b>5.1 Եթե այո, ապա ո՞ր մասնագիտությունների կամ աշխատանքների դեպքում</b></label>
                        <div data-provider="repeater">
                            <div data-repeater-list="2-2-5-1">
                                <div data-repeater-item>
                                    <div class="form-group row">
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-sm-3">
                                            <button type="button" class="btn btn-danger" data-repeater-delete>
                                                <i class="fa fa-minus"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 ml-auto">
                                    <button type="button" class="btn btn-primary" data-repeater-create>
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label><b>Եթե ոչ, ապա անցնել հարց  9-ին</b></label>
                    </div>
                    <div class="form-group">
                        <label><b>6. Ձեզ հաջողվե՞լ է լրացնել թափուր աշխատատեղերը՝ չնայած դժվարություններին</b></label>
                        <table class="table">
                            <tr>
                                <td>Այո, երկար ու ինտենսիվ փնտրտուքներից հետո</td>
                                <td><input type="radio" name="2-2-6"></td>
                            </tr>
                            <tr>
                                <td>Այո, որակավորման նկատմամբ պահանջների նվազեցումից հետո</td>
                                <td><input type="radio" name="2-2-6"></td>
                            </tr>
                            <tr>
                                <td>Այո, ժամկետային աշխատակիցների ընդունման միջոցով</td>
                                <td><input type="radio" name="2-2-6"></td>
                            </tr>
                            <tr>
                                <td>Այո, օտարերկրյա քաղաքացիների ընդունման միջոցով</td>
                                <td><input type="radio" name="2-2-6"></td>
                            </tr>
                            <tr>
                                <td>Այո, առկա աշխատակազմի համար կազմակերպված վերապատրաստման դասընթացների միջոցով</td>
                                <td><input type="radio" name="2-2-6"></td>
                            </tr>
                            <tr>
                                <td>Ոչ</td>
                                <td><input type="radio" name="2-2-6"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="form-group">
                        <label><b>7. Ինչպե՞ս են նշված դժվարություններն ազդել Ձեր ընկերության վրա թափուր աշխատատեղերը լրացնելիս</b></label>
                        <table class="table">
                            <tr>
                                <td>Անհրաժեշտություն առաջացավ հետաձգել կամ մերժել պատվերները</td>
                                <td><input type="radio" name="2-2-7"></td>
                            </tr>
                            <tr>
                                <td>Առկա աշխատակազմն ավելի շատ աշխատեց</td>
                                <td><input type="radio" name="2-2-7"></td>
                            </tr>
                            <tr>
                                <td>Ազդեցություն չունեցավ</td>
                                <td><input type="radio" name="2-2-7"></td>
                            </tr>
                        </table>
                    </div>

                    <div class="form-group">
                        <label><b>8. Ձեր կարծիքով որքա՞ն ժամանակ է պահանջվում անձնակազմի վերապատրաստման համար</b></label>
                        <table class="table">
                            <tr>
                                <td>Ուսուցում մինչև 1 շաբաթ տևողությամբ</td>
                                <td><input type="radio" name="2-2-8"></td>
                            </tr>
                            <tr>
                                <td>Ուսուցում մինչև 1 ամիս տևողությամբ</td>
                                <td><input type="radio" name="2-2-8"></td>
                            </tr>
                            <tr>
                                <td>Ուսուցում մինչև  3 ամիս տևողությամբ</td>
                                <td><input type="radio" name="2-2-8"></td>
                            </tr>
                            <tr>
                                <td>Վերապատրաստում մինչև 6 ամիս տևողությամբ</td>
                                <td><input type="radio" name="2-2-8"></td>
                            </tr>
                            <tr>
                                <td>Դժվարանում եմ պատասխանել</td>
                                <td><input type="radio" name="2-2-8"></td>
                            </tr>
                            <tr>
                                <td>Այլ</td>
                                <td><input type="radio" name="2-2-8"></td>
                            </tr>
                        </table>
                    </div>

                    <h5 class=""><strong>ՄԱՍ 3. ՀԱՐՑԵՐ ԹԱՓՈՒՐ ԱՇԽԱՏԱՏԵՂԵՐԻ ՏԵՍԱԿԻ ԵՎ ԾԱՎԱԼԻ ՄԱՍԻՆ</strong></h5>
                    <div class="form-group">
                        <label><b>9.	Ունե՞ք արդյո՞ք թափուր աշխատատեղեր.</b></label>
                        <table cellpadding="10">
                            <thead>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <th>Քանակ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Այո</td>
                                    <td><input type="radio" name="2-3-9"></td>
                                    <td><input type="number" class="form-control"></td>
                                </tr>
                                <tr>
                                    <td>Ոչ</td>
                                    <td><input type="radio" name="2-3-9"></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <label><b>10. Քանի՞ թափուր աշխատատեղի հայտարարություններ եք գրանցել հետևյալ ուղիներով</b></label>
                        <table class="table">
                            <thead>
                                <tr>
                                    <td></td>
                                    <th>Քանակ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Զբաղվածության պետական գործակալություն</td>
                                    <td><input type="number" class="form-control"></td>
                                </tr>
                                <tr>
                                    <td>Աշխատանքի տեղավորման մասնավոր գործակալությունում</td>
                                    <td><input type="number" class="form-control"></td>
                                </tr>
                                <tr>
                                    <td>Սոցիալական ցանցերում</td>
                                    <td><input type="number" class="form-control"></td>
                                </tr>
                                <tr>
                                    <td>Ընկերների և ծանոթների միջոցով </td>
                                    <td><input type="number" class="form-control"></td>
                                </tr>
                                <tr>
                                    <td>չեմ գրանցել  </td>
                                    <td><input type="number" class="form-control"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <label><b>11.	Ո՞ր աշխատանքների համար են ներկայումս Ձեզ անհրաժեշտ նոր աշխատակիցներ և ո՞ր ժամանակահատվածից (ամիս, ամսաթիվ)</b></label>
                        <div class="row">
                            <div class="col-sm-4">
                                <b>Աշխատատեղի  անվանում</b>
                            </div>
                            <div class="col-sm-3">
                                <b>Ցանկալի կրթություն</b>
                            </div>
                            <div class="col-sm-2">
                                <b>Քանակ</b>
                            </div>
                            <div class="col-sm-2">
                                <b>Աշխատանքի սկիզբ</b>
                            </div>
                        </div>
                        <div data-provider="repeater">
                            <div data-repeater-list="3-11">
                                <div data-repeater-item>
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-sm-3">
                                            <select class="form-control">
                                                @foreach(__('options.education') as $index => $edu)
                                                    <option value="{{ $index }}">{{ $edu }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="number" class="form-control">
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control" data-provide="datepicker">
                                        </div>
                                        <div class="col-sm-1">
                                            <button type="button" class="btn btn-danger" data-repeater-delete>
                                                <i class="fa fa-minus"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-1 ml-auto">
                                    <button type="button" class="btn btn-primary" data-repeater-create>
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h5 class=""><strong>ՄԱՍ 4. ԿԱԶՄԱԿԵՐՊՈՒԹՅԱՆ ԿԱՐՃԱԺԱՄԿԵՏ ԵՎ ՄԻՋՆԱԺԱՄԿԵՏ ԾՐԱԳՐԵՐ  </strong></h5>
                    <div class="form-group">
                        <label for=""><b>12.	Հաջորդող 12 ամիսների ընթացքում  (01.01.2018-01.01.2019թ. ) ձեր ընկերությունում ո՞ր աշխատատեղերի կրճատում է նախատեսվում</b></label>
                        <div class="row">
                            <div class="col-sm-4">
                                <b>Աշխատատեղի  անվանում</b>
                            </div>
                            <div class="col-sm-2">
                                <b>Քանակ</b>
                            </div>
                        </div>
                        <div data-provider="repeater">
                            <div data-repeater-list="4-12">
                                <div data-repeater-item>
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="number" class="form-control">
                                        </div>
                                        <div class="col-sm-1">
                                            <button type="button" class="btn btn-danger" data-repeater-delete>
                                                <i class="fa fa-minus"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 ml-auto">
                                    <button type="button" class="btn btn-primary" data-repeater-create>
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for=""><b>13. Հաջորդող 12 ամիսների համար (01.01.2018-01.01.2019) ո՞ր աշխատանքների համար եք նախատեսում լրացուցիչ աշխատակիցներ ընդունել</b></label>
                        <div class="row">
                            <div class="col-sm-4">
                                <b>Աշխատատեղի  անվանում</b>
                            </div>
                            <div class="col-sm-2">
                                <b>Քանակ</b>
                            </div>
                        </div>
                        <div data-provider="repeater">
                            <div data-repeater-list="4-13">
                                <div data-repeater-item>
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="number" class="form-control">
                                        </div>
                                        <div class="col-sm-1">
                                            <button type="button" class="btn btn-danger" data-repeater-delete>
                                                <i class="fa fa-minus"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 ml-auto">
                                    <button type="button" class="btn btn-primary" data-repeater-create>
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for=""><b>13.1 Հաջորդող 3 տարիների համար կարո՞ղ եք նշել, թե ո՞ր աշխատանքների համար եք նախատեսում լրացուցիչ աշխատակիցներ ընդունել</b></label>
                        <div class="row">
                            <div class="col-sm-4">
                                <b>Աշխատատեղի  անվանում</b>
                            </div>
                            <div class="col-sm-2">
                                <b>Քանակ</b>
                            </div>
                        </div>
                        <div data-provider="repeater">
                            <div data-repeater-list="4-13-1">
                                <div data-repeater-item>
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="number" class="form-control">
                                        </div>
                                        <div class="col-sm-1">
                                            <button type="button" class="btn btn-danger" data-repeater-delete>
                                                <i class="fa fa-minus"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 ml-auto">
                                    <button type="button" class="btn btn-primary" data-repeater-create>
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label><b>14. Ո՞րն է լրացուցիչ աշխատակիցների անհրաժեշտության հիմնական պատճառը</b></label>
                        <table class="table">
                            <tbody>
                            <tr>
                                <td>Շրջանառության աճ</td>
                                <td><input type="radio" name="4-14"></td>
                            </tr>
                            <tr>
                                <td>Նոր տեխնոլոգիաների ներդրում</td>
                                <td><input type="radio" name="4-14"></td>
                            </tr>
                            <tr>
                                <td>Նոր ապրանքների մշակում</td>
                                <td><input type="radio" name="4-14"></td>
                            </tr>
                            <tr>
                                <td>Շուկաների/պատվիրատուների փոփոխում</td>
                                <td><input type="radio" name="4-14"></td>
                            </tr>
                            <tr>
                                <td>Կադրերի հոսունություն/համալրում</td>
                                <td><input type="radio" name="4-14"></td>
                            </tr>
                            <tr>
                                <td>Այլ</td>
                                <td><input type="radio" name="4-14"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <h5>
                        <strong>ՄԱՍ 5. ՄԱՍՆԱԳԻՏՈՒԹՅԱՆ ԳՈՐԾՆԱԿԱՆ ՈՒՍՈՒՑՈՒՄ</strong>
                    </h5>
                    <div class="form-group">
                        <label for="">
                            <b>15. Ինչպե՞ս եք գնահատում Ձեր ընկերությունում աշխատանքի ընդունված  մասնագիտական հաստատությունների շրջանավարտների մասնագիտական որակավորումը</b>
                        </label>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <td></td>
                                <th>Պետական</th>
                                <th>Մասնավոր </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1.Գոհ եմ</td>
                                <td>
                                    <input type="radio" name="q5-15-p">
                                    անցնել հարց 17
                                </td>
                                <td>
                                    <input type="radio" name="q5-15-m">
                                    անցնել հարց 17
                                </td>
                            </tr>
                            <tr>
                                <td>2.Որոշ չափով գոհ եմ</td>
                                <td>
                                    <input type="radio" name="q5-15-p">
                                    անցնել հարց 18
                                </td>
                                <td>
                                    <input type="radio" name="q5-15-m">
                                    անցնել հարց 18
                                </td>
                            </tr>
                            <tr>
                                <td>3.Դժգոհ եմ</td>
                                <td>
                                    <input type="radio" name="q5-15-p">
                                    անցնել հարց 18
                                </td>
                                <td>
                                    <input type="radio" name="q5-15-m">
                                    անցնել հարց 18
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <label><b>16. Եթե ընտրել եք 1 կետի պատասխանը. Գոհ եմ, ապա որո՞նք են ներկայումս Հայաստանի պետական միջին և արհեստագործական մասնագիտական կրթական համակարգի դրական կողմերը</b></label>
                        <table class="table">
                            <tr>
                                <td>Միջին և արհեստագործական մասնագիտական կրթությունը Հայաստանում պետական ուսումնական հաստատություններում հիմնականում անվճար է </td>
                                <td><input type="radio" name="q5-15"></td>
                            </tr>
                            <tr>
                                <td>Միջին և արհեստագործական մասնագիտական կրթությունը Հայաստանում պետական հաստատություններում ավելի բարձր որակ ունի, քան մասնավոր կրթական հաստատություններում</td>
                                <td><input type="radio" name="q5-15"></td>
                            </tr>
                            <tr>
                                <td>Միջին և արհեստագործական մասնագիտական հաստատության շրջանավարտին հեշտությամբ կարելի է սովորեցնել այլ մասնագիտություն</td>
                                <td><input type="radio" name="q5-15"></td>
                            </tr>
                            <tr>
                                <td>Այլ</td>
                                <td><input type="radio" name="q5-15"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="form-group">
                        <label><b>17. Եթե ընտրել եք 2 կամ 3 կետերում նշված պատասխանները (Որոշ չափով գոհ եմ ,  Դժգոհ եմ) , ապա .Ձեր կարծիքով ի՞նչն է թերի ներկայումս Հայաստանի միջին մասնագիտական կրթության մեջ (կարելի է նշել մի քանի պատասխան)</b></label>
                        <table class="table">
                            <tr>
                                <td>1.	Մասնագիտական տեսական գիտելիքները</td>
                                <td><input type="checkbox" name="q5-17"></td>
                            </tr>
                            <tr>
                                <td>2.	Մասնագիտական գործնական գիտելիքները</td>
                                <td><input type="checkbox" name="q5-17"></td>
                            </tr>
                            <tr>
                                <td>3.	Մասնագիտական հմտությունները</td>
                                <td><input type="checkbox" name="q5-17"></td>
                            </tr>
                            <tr>
                                <td>4.	Աշխատանքի նկատմամբ պատասխանատվության ստանձնումը</td>
                                <td><input type="checkbox" name="q5-17"></td>
                            </tr>
                            <tr>
                                <td>5.	Ճշտապահությունը</td>
                                <td><input type="checkbox" name="q5-17"></td>
                            </tr>
                            <tr>
                                <td>6.	Թիմային աշխատանքը</td>
                                <td><input type="checkbox" name="q5-17"></td>
                            </tr>
                            <tr>
                                <td>7.	Ինքնուրույն մտածելակերպը կամ նախաձեռնողականությունը (սեփական մղումով որևէ խնդրի լուծում գտնելը)</td>
                                <td><input type="checkbox" name="q5-17"></td>
                            </tr>
                            <tr>
                                <td>8.	Հաղորդակցվելու ունակությունը</td>
                                <td><input type="checkbox" name="q5-17"></td>
                            </tr>
                            <tr>
                                <td>9.	Ինքնուրույն կրթվելու ունակությունը</td>
                                <td><input type="checkbox" name="q5-17"></td>
                            </tr>
                            <tr>
                                <td>10.	Շարունակական ինքնազարգացումը</td>
                                <td><input type="checkbox" name="q5-17"></td>
                            </tr>
                            <tr>
                                <td>11.	Ձեռնարկատիրական մտածելակերպի բացակայությունը</td>
                                <td><input type="checkbox" name="q5-17"></td>
                            </tr>
                            <tr>
                                <td>12. Այլ</td>
                                <td><input type="checkbox" name="q5-17"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="form-group">
                        <label for="">17.b Ի՞նչ անձնական կարողություններ եք ակնկալում Ձեր ապագա աշխատակիցներից։
                            (Կարելի է նշել մի քանի պատասխան)
                        </label>
                        <table class="table">
                            <tr>
                                <td>
                                    <input type="checkbox" name="q5-17-b">
                                </td>
                                <td>Ճշտապահություն</td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="checkbox" name="q5-17-b">
                                </td>
                                <td>Պատասխանատվություն</td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="checkbox" name="q5-17-b">
                                </td>
                                <td>Թիմում աշխատելու կարողություն</td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="checkbox" name="q5-17-b">
                                </td>
                                <td>Հաղորդակցվելու կարողություն</td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="checkbox" name="q5-17-b">
                                </td>
                                <td>Նախաձեռնողականություն, ինքնուրույնություն</td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="checkbox" name="q5-17-b">
                                </td>
                                <td>Ամբողջ կյանքի ընթացքում սովորելու պատրաստակամություն</td>
                            </tr>
                        </table>
                    </div>
                    <div class="form-group">
                        <label for="">
                            <b>18.	Որքանո՞վ եք կարևորում ՄԿՈւ հաստատության ուսանողի մասնագիտական գործնական ուսուցումը կրթության ընթացքում</b>
                        </label>
                        <table class="table">
                            <tr>
                                <td>Շատ կարևոր է</td>
                                <td><input type="radio" name="q5-18"></td>
                            </tr>
                            <tr>
                                <td>Կարևոր է</td>
                                <td><input type="radio" name="q5-18"></td>
                            </tr>
                            <tr>
                                <td>Կարևոր չէ  </td>
                                <td><input type="radio" name="q5-18"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="form-group">
                        <label for="">
                            <b>19.	Ձեր կարծիքով որտե՞ղ պետք է իրականացվի մասնագիտական գործնական ուսուցումը:(Կարելի է ընտրել մի քանի պատասխան)</b>
                        </label>
                        <table class="table">
                            <tr>
                                <td>ՄԿՈւ հաստատությունում - առանձին ուսումնական արհեստանոցում</td>
                                <td><input type="checkbox" name="q5-19"></td>
                            </tr>
                            <tr>
                                <td>Ընկերություններում</td>
                                <td><input type="checkbox" name="q5-19"></td>
                            </tr>
                            <tr>
                                <td>Ձեր սեփական ընկերությունում</td>
                                <td><input type="checkbox" name="q5-19"></td>
                            </tr>
                            <tr>
                                <td>Արտերկրում</td>
                                <td><input type="checkbox" name="q5-19"></td>
                            </tr>
                            <tr>
                                <td>Հայաստանում գործող մասնագիտական ոլորտի որևէ արտասահմանյան կազմակերպությունում</td>
                                <td><input type="checkbox" name="q5-19"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="form-group">
                        <label for="">
                            <b>20.	Լիարժեք մասնագիտական կրթությունն ավարտելու համար Ձեր կարծիքով որքա՞ն պետք է տևի գործնական ուսուցումը</b>
                        </label>
                        <table class="table">
                            <tr>
                                <td>Չորս ամիս ուսման ժամանակահատվածում</td>
                                <td><input type="radio" name="q5-20"></td>
                            </tr>
                            <tr>
                                <td>Տեսական ուսուցումից հետո երկու շաբաթ</td>
                                <td><input type="radio" name="q5-20"></td>
                            </tr>
                            <tr>
                                <td>Գերմանական երկակի (դուալ) կրթական համակարգի հիմքով՝ 70% մասնագիտական պրակտիկա և 30% մասնագիտության տեսության
                                    ուսուցում քոլեջում)
                                </td>
                                <td><input type="radio" name="q5-20"></td>
                            </tr>
                            <tr>
                                <td>Ուսման յուրաքանչյուր տարում 2-4 շաբաթ
                                    ամառվա արձակուրդների ընթացքում
                                </td>
                                <td><input type="radio" name="q5-20"></td>
                            </tr>
                            <tr>
                                <td>Այլ առաջարկ</td>
                                <td><input type="radio" name="q5-20"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="form-group">
                        <label for="">
                            <b>21.	Որքա՞նով եք կարևորում ՄԿՈւ հաստատության ուսանողի գործնական  ուսումնառության ընթացքում համապատասխան որակավորում ունեցող ուսուցիչ-մասնագետի («ուսուցիչ-վարպետը») ուղղորդումը</b>
                        </label>
                        <table class="table">
                            <tr>
                                <td>Շատ կարևոր է</td>
                                <td><input type="radio" name="q5-21"></td>
                            </tr>
                            <tr>
                                <td>Կարևոր է </td>
                                <td><input type="radio" name="q5-21"></td>
                            </tr>
                            <tr>
                                <td>Կարևոր չէ
                                </td>
                                <td><input type="radio" name="q5-21"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="form-group">
                        <label for="">
                            <b>22. «Ուսուցիչ-վարպետը» ի՞նչ որակավորումներ պետք է ունենա (կարելի է ընտրել մի քանի պատասխան)</b>
                        </label>
                        <table class="table">
                            <tr>
                                <td>Ինքը լինի ձեռնարկատեր</td>
                                <td><input type="checkbox" name="q5-22"></td>
                            </tr>
                            <tr>
                                <td>Փորձառություն երիտասարդների հետ աշխատանքում</td>
                                <td><input type="checkbox" name="q5-22"></td>
                            </tr>
                            <tr>
                                <td>Դիդակտիկ հմտություններ (իմանա՝ ինչը ինչպես պետք է արվի, ունենա աշխատանքային քայլերը բացատրելու, դրանք ցույց տալու և այնուհետև աշխատանքների իրականացումը հսկելու/ուղղելու ունակություն)</td>
                                <td><input type="checkbox" name="q5-22"></td>
                            </tr>
                            <tr>
                                <td>Հանձնարարությունը տվողների և կատարողների (ՄԿՈւ հաստատության ուսանողներ) միջև համաձայնությունները կառավարելու ունակություն</td>
                                <td><input type="checkbox" name="q5-22"></td>
                            </tr>
                            <tr>
                                <td>Նախկինում որևէ ՄԿՈւ հաստատությունում աշխատանքային փորձ ունենա որպես ուսուցիչ</td>
                                <td><input type="checkbox" name="q5-22"></td>
                            </tr>
                            <tr>
                                <td>Ունենա կազմակերպությունում բոլոր գործընթացների ուսուցման և գործնական պարապմունքների անցկացման իմացություն, ինչպես նաև տիրապետի տվյալ կազմակերպության պահանջներին</td>
                                <td><input type="checkbox" name="q5-22"></td>
                            </tr>
                            <tr>
                                <td>Համապատասխան մասնագիտության համար տեսական կրթական ծրագրի և կազմակերպությունում դրա համապատասխան պրակտիկ կիրառության իմացություն</td>
                                <td><input type="checkbox" name="q5-22"></td>
                            </tr>
                            <tr>
                                <td>Ուսուցիչ-վարպետը պետք է իր որակավորումը մշտապես ընդլայնի (ցանկալի է համապատասխան վկայականներով/դիպլոմներով)</td>
                                <td><input type="checkbox" name="q5-22"></td>
                            </tr>
                            <tr>
                                <td>Այլ</td>
                                <td><input type="checkbox" name="q5-22"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="form-group">
                        <label><b>23.	Արդյո՞ք ՄԿՈւ հաստատության ուսանողն իր մասնագիտական գործնական ուսումնառության ընթացքում պետք է ստանա ուսումնական վարձատրություն</b></label>
                        <div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="q5-23y" name="q5-23">
                                <label class="form-check-label" for="q5-23-y">Այո</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="q5-23" id="q5-23-n">
                                <label class="form-check-label" for="q5-23-n">Ոչ</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">
                            <b>Եթե այո, ապա որքա՞ն պետք է կազմի այդ վարձատրությունը (Իր գործնական ուսումնառության ընթացքում ՄԿՈՒ հաստատության ուսանողը որոշակի չափով բարձրացնում է կազմակերպության արտադրողականությունը: Այդ պատճառով Ձեզ համար որպես ձեռնարկատեր կամ որպես մարդկային ռեսուրսների (ՄՌ) կառավարիչ կարևոր է իմանալ, թե ինչպես եք վարձատրելու ուսանողին իր կողմից ավելացած արժեք ստեղծելու համար)</b>
                        </label>
                        <input type="number" class="form-control">
                    </div>
                    <div class="form-group">
                        <label><b>24.	Պե՞տք է արդյոք ուսուցիչ-վարպետը ՄԿՈւ հաստատության ուսանողի մասնագիտության գործնական ուսուցման ընթացքում ստանա լրացուցիչ վարձատրություն</b></label>
                        <div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="q5-24y" name="q5-24">
                                <label class="form-check-label" for="q5-24-y">Այո</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="q5-24" id="q5-24-n">
                                <label class="form-check-label" for="q5-24-n">Ոչ</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">
                            <b>Եթե այո, ապա որքա՞ն պետք է կազմի վարձատրությունը:
                                (Ուսման պրակտիկայի ժամանակ վարպետ- ուսուցիչը  վերապատրաստում է ՄԿՈւ հաստատության ուսանողին, ով հավանաբար կարող է Ձեր կազմակերպության նոր աշխատող դառնալ: Դա Ձեր կազմակերպության համար կարող է որոշակի չափով հեռանկարային լինել արտադրողականության բարձրացման առումով:  Այդ պատճառով Ձեզ համար որպես ձեռնարկատեր կամ որպես ՄՌ-կառավարիչ կարևոր է իմանալ, թե ինչպես եք վարձատրելու վարպետ- ուսուցչին ավելացած արժեք ստեղծելու համար)
                            </b>
                        </label>
                        <input type="number" class="form-control">
                    </div>
                    <div class="form-group">
                        <label><b>25. Առաջարկու՞մ եք արդյոք ներկայումս  ՄԿՈւ հաստատության ուսանողներին մասնագիտական գործնական պարապմունք</b></label>
                        <div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="q5-25y" name="q5-25">
                                <label class="form-check-label" for="q5-25-y">Այո</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="q5-25" id="q5-25-n">
                                <label class="form-check-label" for="q5-25-n">Ոչ</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label><b>26.	Ձեր կարծիքով ապագայում կառաջարկեք տեղեր պրակտիկա անցկացնելու (մասնագիտության գործնական ուսուցման) համար</b></label>
                        <div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="q5-26y" name="q5-26">
                                <label class="form-check-label" for="q5-26-y">Այո</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="q5-26" id="q5-26-n">
                                <label class="form-check-label" for="q5-26-n">Ոչ</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">
                            <b>27. Խնդրում ենք առավել մանրամասն նկարագրեք պրակտիկայի անցկացման գործընթացը</b>
                        </label>
                        <table class="table">
                            <thead>
                                <tr>
                                    <td></td>
                                    <td>Այո</td>
                                    <td>Ոչ</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>Պրակտիկանտը գործնական պարապմունքի սկզբում ստանում է տեղեկատվություն կազմակերպության գործընթացների մասին</th>
                                    <td><input type="radio" name="q5-27-1"></td>
                                    <td><input type="radio" name="q5-27-1"></td>
                                </tr>
                                <tr>
                                    <th>Պրակտիկանտի հետ անցկացվում  է անձնական հարցազրույց</th>
                                    <td><input type="radio" name="q5-27-2"></td>
                                    <td><input type="radio" name="q5-27-2"></td>
                                </tr>
                                <tr>
                                    <th>Պրակտիկանտին տրվում է վարպետ- ուսուցիչ (կամ մեկ այլ համապատասխան մասնագետ հարցերի համար)</th>
                                    <td><input type="radio" name="q5-27-3"></td>
                                    <td><input type="radio" name="q5-27-3"></td>
                                </tr>
                                <tr>
                                    <th>Ակնկալվում է, որ պրակտիկանտը կդիմի ֆորմալ ձևով, օր.՝ կներկայացնի CV,  հետաքրքրություններ,  կազմակերպության հետ աշխատելու մոտիվացիոն նամակ, բնութագրեր այլ անձանցից /աշխատավայրերից  կամ ՄԿՈւ հաստատության անունից)</th>
                                    <td><input type="radio" name="q5-27-4"></td>
                                    <td><input type="radio" name="q5-27-4"></td>
                                </tr>
                                <tr>
                                    <th>Պրակտիկայի ժամկետը կազմում է 2-4 շաբաթ</th>
                                    <td><input type="radio" name="q5-27-5"></td>
                                    <td><input type="radio" name="q5-27-5"></td>
                                </tr>
                                <tr>
                                    <th>Պրակտիկայի ժամկետը կազմում է 1 ամսից ավել (և կարող է ընթանալ նաև ուսումնական արձակուրդների ժամանակ)</th>
                                    <td><input type="radio" name="q5-27-6"></td>
                                    <td><input type="radio" name="q5-27-6"></td>
                                </tr>
                                <tr>
                                    <th>Աշխատանքի դրական գնահատականի դեպքում պրակտիկանտը ստանում է համապատասխան վարձատրություն  </th>
                                    <td><input type="radio" name="q5-27-7"></td>
                                    <td><input type="radio" name="q5-27-7"></td>
                                </tr>
                                <tr>
                                    <th>Պրակտիկայի տեղեր տրամադրելիս, հնարավոր է ապագա աշխատողներին աշխատանքի ընդունելուց առաջ փորձաշրջան տրամադրել  և կազմակերպությունում հետագա աշխատանքի համար նրանց նախապատրաստել</th>
                                    <td><input type="radio" name="q5-27-8"></td>
                                    <td><input type="radio" name="q5-27-8"></td>
                                </tr>
                                <tr>
                                    <th>Գրանցվել ՄԿՈւ հաստատությունների և կազմակերպությունների միջև ապագայում ստեղծվելիք պրակտիկայի տեղերի տրամադրման տվյալների բազայում ընդգրկվելու համար</th>
                                    <td><input type="radio" name="q5-27-9"></td>
                                    <td><input type="radio" name="q5-27-9"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <label for="">
                            <b>28.	Ձեր ակնկալիքները մասնագիտական գործնական կրթությունից</b>
                        </label>
                        <table class="table">
                            <thead>
                            <tr>
                                <td></td>
                                <td>Այո</td>
                                <td>Ոչ</td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th>Ակնկալվում է ունենալ ավելի լավ որակավորված սկսնակ մասնագետ, որպես ապագա աշխատող՝ լրացուցիչ պրակտիկա (կամ երկակի մասնագիտական ուսուցում) անցնելուց հետո</th>
                                <td><input type="radio" name="q5-28-1"></td>
                                <td><input type="radio" name="q5-28-1"></td>
                            </tr>
                            <tr>
                                <th>Այլ</th>
                                <td><input type="radio" name="q5-28-2"></td>
                                <td><input type="radio" name="q5-28-2"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <label><b>29.	Պատրաստ կլինե՞ք մեկ օրով բացելու Ձեր ընկերության դռները հետաքրքրվող պատանիների (կամ նաև ծնողների) և ՄԿՈւ հաստատության ներկայացուցիչների համար որպես պոտենցիալ գործատու (« Մասնագիտական կողմնորոշման օր» կազմակերպելու համար)</b></label>
                        <div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="q5-29y" name="q5-29">
                                <label class="form-check-label" for="q5-29-y">Այո</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="q5-29" id="q5-29-n">
                                <label class="form-check-label" for="q5-29-n">Ոչ</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label><b>31. Տրամադրո՞ւմ եք արդյոք սկսնակ մասնագետներին աշխատանքային պրակտիկայի տեղեր Հայաստանի աշխատանքային իրավունքի համաձայն</b></label>
                        <div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="q5-31y" name="q5-31">
                                <label class="form-check-label" for="q5-31-y">Այո</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="q5-31" id="q5-31-n">
                                <label class="form-check-label" for="q5-31-n">Ոչ</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label><b>32. Ո՞ր մասնագիտություններով եք առաջարկում աշխատանքային պրակտիկան</b></label>
                        <div data-provider="repeater">
                            <div data-repeater-list="q5-32">
                                <div data-repeater-item>
                                    <div class="form-group row">
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-sm-6">
                                            <button type="button" class="btn btn-danger" data-repeater-delete>
                                                <i class="fa fa-minus"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 ml-auto">
                                    <button type="button" class="btn btn-primary" data-repeater-create>
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">
                            <b>33. Ի՞նչ ակնկալիքներ/առաջարկություններ ունեք պետական կառույցներից ՄԿՈւ ոլորտի բարելավման ուղղությամբ</b>
                        </label>
                        <textarea name="" id="" cols="30" rows="10" class="form-control"></textarea>
                    </div>
                    <h5 class="text-center">
                        <strong>ՇՆՈՐՀԱԿԱԼՈՒԹՅՈՒՆ ՄԱՍՆԱԿՑՈՒԹՅԱՆ ՀԱՄԱՐ</strong>
                    </h5>

                    <div class="text-right">
                        <a href="/" class="btn btn-success">Ավարտել</a>
                    </div>
                </form>
            </div>
        </main>

        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('theme/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('theme/vendor/bootstrap-datepicker/bootstrap-datepicker.'. App::getLocale() .'.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap-datepicker.init.js') }}"></script>
        <script src="{{ asset('js/jquery.repeater.init.js') }}"></script>
    </body>
</html>