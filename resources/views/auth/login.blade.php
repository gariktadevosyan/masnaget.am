@extends('layouts.auth')
@section('title', __('auth.login'))
@section('content')
<div class="login-content">
    <div class="login-logo">
        <a href="{{ config('app.url') }}">
            <img src="https://via.placeholder.com/480x59.png?text={{ config('app.name') }}" alt="{{ config('app.name') }}">
        </a>
    </div>
    <div class="login-form">
        <form action="{{ route('login') }}" method="post" novalidate>
            @csrf
            <div class="form-group">
                <label for="email">@lang('auth.email')</label>
                <input
                        class="form-control au-input au-input--full @if($errors->has('email')){{'is-invalid'}}@endif"
                        type="email"
                        name="email"
                        id="email"
                        value="{{ old('email') }}"
                        autofocus
                >
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="password">@lang('auth.password')</label>
                <input
                        id="password"
                        class="au-input au-input--full form-control @if($errors->has('password')){{'is-invalid'}}@endif"
                        type="password"
                        name="password"
                >
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>
            <div class="login-checkbox">
                <div class="custom-control custom-checkbox">
                    <input
                            type="checkbox"
                            class="custom-control-input"
                            name="remember"
                            id="remember"
                            {{ old('remember') ? 'checked' : '' }}
                    >
                    <label class="custom-control-label" for="remember">@lang('auth.stay_logged_in')</label>
                </div>
                <label>
                    <a href="{{ route('password.request') }}"><small>@lang('auth.forgot_password')</small></a>
                </label>
            </div>
            <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">@lang('auth.login')</button>
        </form>
        <div class="register-link">
            <p>
                <a href="{{ route('register') }}">@lang('auth.register')</a>
            </p>
        </div>
    </div>
</div>
@endsection