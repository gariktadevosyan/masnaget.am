@extends('layouts.auth')
@section('title', __('auth.reset_password'))
@section('content')
<div class="login-content">
    <div class="login-logo">
        <a href="{{ config('app.url') }}">
            <img src="https://via.placeholder.com/480x59.png?text={{ config('app.name') }}" alt="{{ config('app.name') }}">
        </a>
    </div>
    <div class="login-form">
        <form action="{{ route('password.email') }}" method="post" novalidate>
            @csrf
            @if (session('status'))
                <p>{{ session('status') }}</p>
            @endif
            <div class="form-group">
                <label for="email">@lang('auth.email')</label>
                <input
                        class="form-control au-input au-input--full @if($errors->has('email')){{'is-invalid'}}@endif"
                        type="email"
                        name="email"
                        id="email"
                        value="{{ old('email') }}"
                        autofocus
                >
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
            <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">
                @lang('auth.request')
            </button>
        </form>
        <div class="register-link">
            <p>
                <a href="{{ route('register') }}">@lang('auth.register')</a>
            </p>
        </div>
    </div>
</div>
@endsection