@extends('layouts.auth')
@section('title', __('auth.change_password'))
@section('content')
<div class="login-content">
    <div class="login-logo">
        <a href="{{ config('app.url') }}">
            <img src="https://via.placeholder.com/480x59.png?text={{ config('app.name') }}" alt="{{ config('app.name') }}">
        </a>
    </div>
    <div class="login-form">
        <form action="{{ route('password.update') }}" method="post" novalidate>
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="form-group">
                <label for="email">@lang('auth.email')</label>
                <input
                    class="form-control au-input au-input--full @if($errors->has('email')){{'is-invalid'}}@endif"
                    type="email"
                    name="email"
                    id="email"
                    value="{{ $email ?? old('email') }}"
                    autofocus
                >
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="password">@lang('auth.new_password')</label>
                <input
                    class="form-control au-input au-input--full @if($errors->has('password')){{'is-invalid'}}@endif"
                    type="password"
                    name="password"
                    id="password"
                >
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="password_confirmation">@lang('auth.password_confirmation')</label>
                <input
                    class="form-control au-input au-input--full"
                    type="password"
                    name="password_confirmation"
                    id="password_confirmation"
                >
            </div>
            <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">
                @lang('auth.change_password')
            </button>
            <div class="login-checkbox justify-content-center">
                <label>
                    <a href="{{ route('password.request') }}"><small>@lang('auth.request')</small></a>
                </label>
            </div>
        </form>
    </div>
</div>
@endsection