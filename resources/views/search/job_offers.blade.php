@extends('layouts.client')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 pt-2">
                <form action="/search" method="get">
                    <div class="form-group">
                        <input type="text" name="kwd" class="form-control" value="{{ array_has($search, 'kwd') ? $search['kwd'] : '' }}" placeholder="Բանալի բառեր․․․">
                    </div>
                    <div class="form-group">
                        <place-search
                                province="{{ array_has($search, 'province') ? $search['province'] : 0 }}"
                                community="{{ array_has($search, 'community') ? $search['community'] : 0 }}"
                                {{ array_has($search, 'othercountry') ? 'othercountry' : '' }}
                        ></place-search>
                    </div>
                    <div class="form-group">
                        <select name="sphere" class="form-control">
                            <option value="na">-- ոլորտ --</option>
                            @foreach($spheres as $sphere)
                                <option
                                        value="{{ $sphere->id }}"
                                        {{ array_has($search, 'sphere') && $search['sphere'] == $sphere->id ? 'selected' : '' }}
                                >{{ $sphere->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <select name="education" class="form-control">
                            <option value="na">-- կրթություն --</option>
                            @foreach (__('options.education') as $key => $value)
                                <option
                                        value="{{ $key }}"
                                        {{ array_has($search, 'education') && $search['education'] === (string)$key ? 'selected' : '' }}
                                >{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <select name="experience" class="form-control">
                            <option value="na">-- աշխ․ փորձ --</option>
                            @foreach (__('options.experiences') as $key => $value)
                                <option
                                        value="{{ $key }}"
                                        {{ array_has($search, 'experience') && $search['experience'] === (string)$key ? 'selected' : '' }}
                                >{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-sm btn-primary">Փնտրել</button>
                    </div>
                </form>
            </div>
            <div class="col-lg-9">
                <job-listing :search="{{ json_encode($search) }}"></job-listing>
            </div>
        </div>
    </div>
@endsection