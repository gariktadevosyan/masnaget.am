@extends('layouts.client')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-4 pt-2">
                <form action="/search/cv" method="get">
                    <div class="form-group">
                        <label>Տարիք</label>
                        <div class="input-group">
                            <input type="number" name="age[from]" class="form-control" value="{{ array_has($search, 'age') ? $search['age']['from'] : '' }}">
                            <div class="input-group-prepend">
                                <span class="input-group-text"> -ից</span>
                            </div>
                            <input type="number" name="age[to]" class="form-control" value="{{ array_has($search, 'age') ? $search['age']['to'] : '' }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <place-search
                                province="{{ array_has($search, 'province') ? $search['province'] : 0 }}"
                                community="{{ array_has($search, 'community') ? $search['community'] : 0 }}"
                                {{ array_has($search, 'othercountry') ? 'othercountry' : '' }}
                        ></place-search>
                    </div>
                    <div class="form-group">
                        <div class="mb-2">
                            <select name="experience[sphere]" class="form-control">
                                <option value="na">-- ոլորտ --</option>
                                @foreach($spheres as $sphere)
                                    <option
                                            value="{{ $sphere->id }}"
                                            {{ array_has($search, 'experience') && $search['experience']['sphere'] === (string)$sphere->id ? 'selected' : '' }}
                                    >{{ $sphere->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <select name="experience[period]" class="form-control">
                            <option value="na">-- փորձ --</option>
                            @foreach (__('options.experiences') as $key => $value)
                                <option
                                        value="{{ $key }}"
                                        {{ array_has($search, 'experience') && $search['experience']['period'] === (string)$key ? 'selected' : '' }}
                                >{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <select name="education" class="form-control">
                            <option value="na">-- կրթություն --</option>
                            @foreach (__('options.education') as $key => $value)
                                <option
                                        value="{{ $key }}"
                                        {{ array_has($search, 'education') && $search['education'] === (string)$key ? 'selected' : '' }}
                                >{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-sm btn-primary">Փնտրել</button>
                    </div>
                </form>
            </div>
            <div class="col-lg-8">
                <table class="table">
                    @foreach($cvs as $cv)
                        <tr>
                            <td><a href="/cv/{{ $cv->id }}" target="_blank">{{ $cv->name }}</a></td>
                            <td>{{ $cv->birth_date }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection