<table border="1" style="border-collapse: collapse; font-size: 12px;" cellpadding="4" align="center" width="90%">
    <thead>
        <tr>
            @if($cv->avatar)
            <td>
                <img src="{{ $cv->avatar }}">
            </td>
            @endif
            <th @if(!$cv->avatar) colspan="2" @endif>
                <h3>{{ $cv->name }}</h3>
                <p><small>{{ $user->email }}</small></p>
                @if(!empty($cv->phone))
                    <p><small>
                            @foreach($cv->phone as $phone)
                                {{ $phone['phone'] . ' ' }}
                            @endforeach
                        </small></p>
                @endif
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th>@lang('cv.birth_date')</th>
            <td>{{ $cv->birth_date }}</td>
        </tr>
        <tr>
            <th>@lang('cv.place')</th>
            <td>
                @if ($cv->community_id){{ $cv->community->region->province->name . ', ' . $cv->community->region->name . ', ' . $cv->community->name }} @if ($cv->address) <br> @endif @endif
                @if ($cv->address) {{ $cv->address }}@endif
            </td>
        </tr>
        <tr>
            <th>@lang('cv.education')</th>
            <td align="center">
                <p>
                    <strong>@lang('options.education.' . $cv->education)</strong>
                </p>
                <br>
                <table border="1" style="border-collapse: collapse" cellpadding="3" width="100%">
                    <thead>
                        <tr>
                            <th>@lang('cv.institution')</th>
                            <th>@lang('cv.profession')</th>
                            <th>@lang('cv.start_date')</th>
                            <th>@lang('cv.end_date')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($cv->education_details as $edu)
                            <tr>
                                <td>{{ $edu['institution'] }}</td>
                                <td>{{ $edu['profession'] }}</td>
                                <td>{{ $edu['start_date'] }}</td>
                                <td>{{ $edu['end_date'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <th>@lang('cv.languages')</th>
            <td>
                {{ $cv->languagesAsString }}
            </td>
        </tr>
        <tr>
            <th>@lang('cv.desired_sphere') <small>(@lang('cv.sphere_experience'))</small></th>
            <td>
                <ul>
                    @foreach($cv->spheres as $sphere)
                    <li>{{ $sphere->title }} <small>({{ __('options.experiences.' . $sphere->pivot->experience) }})</small></li>
                    @endforeach
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <p>
                    <strong>@lang('cv.experience')</strong>
                </p>
                <br>
                <table border="1" style="border-collapse: collapse" cellpadding="3" width="100%">
                    <thead>
                        <tr>
                            <th>@lang('cv.organization')</th>
                            <th>@lang('cv.sphere')</th>
                            <th>@lang('cv.position')</th>
                            <th>@lang('cv.start_date')</th>
                            <th>@lang('cv.end_date')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($cv->experiences as $experience)
                            <tr>
                                <td>{{ $experience->organization }}</td>
                                <td>{{ $experience->sphere->title }}</td>
                                <td>{{ $experience->position }}</td>
                                <td>{{ $experience->start_date }}</td>
                                <td>{{ $experience->end_date }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <th>@lang('cv.skills')</th>
            <td>{!! $cv->skills !!}</td>
        </tr>
        <tr>
            <th>@lang('cv.military_book')</th>
            <td>
                <img src="{{ $cv->military_book ? asset('img/icon_check.png') : asset('img/icon_uncheck.png') }}" height="14">
            </td>
        </tr>
        <tr>
            <th>@lang('cv.drive_right')</th>
            <td>
                <img src="{{ $cv->drive_right ? asset('img/icon_check.png') : asset('img/icon_uncheck.png') }}" height="14">
            </td>
        </tr>
    </tbody>
</table>
