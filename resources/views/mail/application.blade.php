<table width="100%">
    <tr>
        <td>
            <table width="400" align="center">
                <tr>
                    <th colspan="2">
                        <a href="{{ route('offer.show', [$jobOffer->id]) }}">{{ $jobOffer->title }}</a>
                    </th>
                </tr>
                <tr>
                    <td>Սկիզբ</td>
                    <td>{{ $jobOffer->opening_date }}</td>
                </tr>
                <tr>
                    <td>Վերջնաժամկետ</td>
                    <td>{{ $jobOffer->deadline }}</td>
                </tr>
                <tr>
                    <td>Թարմացվել է</td>
                    <td>{{ $jobOffer->updated_at }}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table width="800" align="center">
                <tr>
                    <th><h3>Մոտիվացիոն նամակ</h3></th>
                </tr>
                <tr>
                    <th>Ինչու՞ եք դիմել այս կազմակերպություն</th>
                </tr>
                <tr>
                    <td>{{ $motivation['company'] }}</td>
                </tr>
                <tr>
                    <th>Ինչու՞ եք դիմել այս աշխատատեղին</th>
                </tr>
                <tr>
                    <td>{{ $motivation['job'] }}</td>
                </tr>
                <tr>
                    <th>Ինչու՞ մենք պետք է ընտրենք Ձեզ</th>
                </tr>
                <tr>
                    <td>{{ $motivation['you'] }}</td>
                </tr>
            </table>
        </td>
    </tr>
</table>