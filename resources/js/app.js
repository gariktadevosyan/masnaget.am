require('./bootstrap');
require('jquery.repeater');
require('inputmask/dist/jquery.inputmask.bundle');
require('inputmask/dist/inputmask/bindings/inputmask.binding.js');
window.Vue = require('vue');
window.Stepper = require('bs-stepper');
window.Croppie = require('croppie');
window.Toastr = require('toastr');
import { Confirmation } from 'bootstrap-confirmation2';
import PlaceSelect from './components/PlaceSelect';
import ImgUpload from './components/ImgUpload';
import ButtonSwitch from './components/ButtonSwitch';
import JobListing from './components/JobListing';
import PlaceSearch from './components/PlaceSearch';

const vm = new Vue({
    el: '#app',
    components: {
        PlaceSelect,
        ImgUpload,
        ButtonSwitch,
        JobListing,
        PlaceSearch
    }
});