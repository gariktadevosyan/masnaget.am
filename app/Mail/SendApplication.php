<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\JobOffer;
use App\User;
use Illuminate\Support\Facades\View;
use Mpdf\Mpdf;

class SendApplication extends Mailable
{
    use Queueable, SerializesModels;

    public $jobOffer;
    public $user;
    public $motivation;

    public function __construct(JobOffer $jobOffer, User $user, $motivation)
    {
        $this->jobOffer = $jobOffer;
        $this->user = $user->load([
            'cv' => function ($query) {
                $query->with([
                    'community' => function ($query) {
                            $query->with(['region' => function ($query) {
                                $query->with(['province']);
                            }]);
                        },
                    'experiences' => function ($query) {
                        $query->with(['sphere']);
                    }
                ]);
            }],
            'spheres'
        );
        $this->motivation = $motivation;
    }

    public function build()
    {
        $attachment = $this->getCvAttachment();

        return $this
            ->from($this->user->email, $this->user->cv->name)
            ->subject('CV - ' . $this->jobOffer->title)
            ->view('mail.application')
            ->attach($attachment, [
                'as' => $this->user->cv->name . ' - CV.pdf',
                'mime' => 'application/pdf',
            ]);
    }

    private function getCvAttachment()
    {
        $view = View::make('pdf.cv', ['user' => $this->user, 'cv' => $this->user->cv]);
        $html = $view->render();

        $mpdf = new Mpdf(['tempDir' => sys_get_temp_dir().DIRECTORY_SEPARATOR.'mpdf']);
        $mpdf->text_input_as_HTML = true;
        $mpdf->WriteHTML($html);
        $fileName = storage_path('app/public/tmp_pdf/cv/cv_pdf_' . $this->user->id . '.pdf');
        $mpdf->output($fileName, 'F');

        return $fileName;
    }
}
