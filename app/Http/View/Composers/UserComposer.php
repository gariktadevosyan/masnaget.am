<?php

namespace App\Http\View\Composers;

use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class UserComposer
{
    public function compose(View $view)
    {
        $user = Auth::user();
        $userDetails = null;

        if (!(is_null($user) || $user->isRole('employee'))) {
            if ($user->isRole('employer')) {
                $userDetails = $user->employerDetails()->first(['name', 'email', 'logo']);
            }
        }

        $view->with([
            'user' => $user,
            'userDetails' => $userDetails
        ]);
    }
}