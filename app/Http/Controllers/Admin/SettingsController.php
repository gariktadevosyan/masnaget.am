<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\StoreSettingsRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\ActivitySphere;
use App\EmployerDetails;

class SettingsController extends Controller
{
    private $activitySphere;

    public function __construct(ActivitySphere $activitySphere)
    {
        $this->middleware('details')->except(['store', 'show', 'showQuestionary']);

        $this->activitySphere = $activitySphere;
    }

    public function index(Request $request)
    {
        $user = $request->user();

        return view('admin.settings.' . $user->role->title, [
            'user' => $user,
            'settings' => $user->employerDetails,
            'activitySpheres' => $this->activitySphere->all()
        ]);
    }

    public function store(StoreSettingsRequest $request)
    {
        $user = $request->user();

        $logo = explode(',', $request->input('logo'));
        $fileName = $this->generateFileName($user->id);
        Storage::put('logos/'. $fileName .'.png', base64_decode($logo[1]));

        $create = $request->except(['_token', '_method']);
        $create['logo'] = $fileName;

        $user->employerDetails()->create($create);

        return redirect(route('admin.index'));
    }

    public function update(StoreSettingsRequest $request)
    {
        $user = $request->user()->load(['employerDetails']);
        $details = $user->employerDetails;

        $update = $request->except(['_token', '_method']);

        if ($request->has('logo')) {
            Storage::delete('logos/'. $details->getOriginal('logo') .'.png');
            $logo = explode(',', $request->input('logo'));
            $fileName = $this->generateFileName($user->id);
            Storage::put('logos/'. $fileName .'.png', base64_decode($logo[1]));
            $update['logo'] = $fileName;
        }

        $update['community_id'] = $request->input('community_id', null);
        $details->update($update);

        return back()->with(['saved' => true]);
    }

    public function show(EmployerDetails $employerDetails)
    {
        return view('client.job_offer.employer', [
            'employer' => $employerDetails
        ]);
    }

    public function showQuestionary()
    {
        return view('auth.details.questionary', [
            'spheres' => $this->activitySphere->all()
        ]);
    }
}
