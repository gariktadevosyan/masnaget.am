<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ActivitySphere;

class DetailsController extends Controller
{
    private $activitySphere;

    public function __construct(ActivitySphere $activitySphere)
    {
        $this->activitySphere = $activitySphere;
    }

    public function __invoke(Request $request)
    {
        $user = $request->user();

        if ($user->employerDetails()->count()) {
            return redirect(route('admin.index'));
        }

        return view('auth.details.' . $user->role->title, [
            'activitySpheres' => $this->activitySphere->all(),
            'user' => $user
        ]);
    }
}
