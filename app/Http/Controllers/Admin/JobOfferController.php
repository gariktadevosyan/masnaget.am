<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreJobOfferRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\JobOffer;
use App\ActivitySphere;

class JobOfferController extends Controller
{
    private $jobOffer;
    private $activitySphere;

    public function __construct(JobOffer $jobOffer, ActivitySphere $activitySphere)
    {
        $this->authorizeResource(JobOffer::class, 'job_offer');

        $this->jobOffer = $jobOffer;
        $this->activitySphere = $activitySphere;
    }

    public function index(Request $request)
    {
        if($request->ajax()) {
            return $this->getJsonList($request);
        }

        return view('admin.job_offers.index', [
            'archiveCount' => $request->user()->jobOffers()->onlyTrashed()->count()
        ]);
    }

    public function create(Request $request)
    {
        $user = $request->user();

        return view('admin.job_offers.create', [
            'spheres' => $this->activitySphere->all(),
            'details' => $user->employerDetails()->first(['activity_sphere_id', 'community_id', 'address'])
        ]);
    }

    public function store(StoreJobOfferRequest $request)
    {
        $jobOffer = $this->jobOffer->fill($request->except(['_token']));
        $request->user()->jobOffers()->save($jobOffer);
        return redirect(route('job-offers.index'));
    }

    public function show(JobOffer $jobOffer)
    {
        $jobOffer->load([
            'employer' => function ($query) {
                $query->select(['users.id'])
                    ->with(['employerDetails' => function ($query) {
                        $query->select(['employer_details.id', 'employer_details.user_id', 'employer_details.name', 'employer_details.logo']);
                    }]);
            },
            'community' => function ($query) {
                $query->with(['region' => function ($query) {
                    $query->with(['province']);
                }]);
            },
            'activitySphere'
        ]);

        return view('client.job_offer.show', [
            'offer' => $jobOffer
        ]);
    }

    public function edit(JobOffer $jobOffer)
    {
        return view('admin.job_offers.edit',[
            'offer' => $jobOffer,
            'spheres' => $this->activitySphere->all()
        ]);
    }

    public function update(StoreJobOfferRequest $request, JobOffer $jobOffer)
    {
        $update = $request->except(['_token', '_method']);
        $update['community_id'] = $request->input('community_id', null);

        $jobOffer->fill($update);
        $jobOffer->save();
        return redirect(route('job-offers.index'));
    }

    public function destroy(JobOffer $jobOffer)
    {
        $jobOffer->delete();
    }

    public function archive(Request $request)
    {
        if ($request->ajax()) {
            return $this->getJsonArchiveList($request);
        }

        return view('admin.job_offers.archive');
    }

    public function restore($id)
    {
        $jobOffer = $this->jobOffer->withTrashed()->find($id);
        $restored = $jobOffer->replicate();
        $restored->deleted_at = null;
        $restored->save();

        return $restored->id;
    }

    private function getJsonList($request)
    {
        $recordsTotal = $recordsFiltered = 0;
        $search = $request->input('search')['value'];

        $data = $request->user()->jobOffers()
            ->tap(function ($query) use (&$recordsTotal, &$recordsFiltered) {
                $recordsTotal = $recordsFiltered = $query->count();
            })
            ->when($search, function ($query) use ($search, &$recordsFiltered) {
                $query->where('title', 'LIKE', '%'. $search .'%');
                $recordsFiltered = $query->count();
            })
            ->skip($request->input('start'))
            ->take($request->input('length'))
            ->orderBy($request->input('columns')[$request->input('order')[0]['column']]['name'], $request->input('order')[0]['dir'])
            ->get(['id', 'title', 'opening_date', 'deadline', 'updated_at']);


        return [
            'draw' => (int)$request->draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data
        ];
    }

    private function getJsonArchiveList($request)
    {
        $recordsTotal = $recordsFiltered = 0;
        $search = $request->input('search')['value'];

        $data = $request->user()->jobOffers()
            ->onlyTrashed()
            ->tap(function ($query) use (&$recordsTotal, &$recordsFiltered) {
                $recordsTotal = $recordsFiltered = $query->count();
            })
            ->when($search, function ($query) use ($search, &$recordsFiltered) {
                $query->where('title', 'LIKE', '%'. $search .'%');
                $recordsFiltered = $query->count();
            })
            ->skip($request->input('start'))
            ->take($request->input('length'))
            ->orderBy($request->input('columns')[$request->input('order')[0]['column']]['name'], $request->input('order')[0]['dir'])
            ->get(['id', 'title', 'opening_date', 'deadline', 'deleted_at']);


        return [
            'draw' => (int)$request->draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data
        ];
    }
}
