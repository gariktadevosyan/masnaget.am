<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Province;
use App\Region;
use App\Community;

class PlaceController extends Controller
{
    private $province;
    private $region;
    private $community;

    public function __construct(Province $province, Region $region, Community $community)
    {
        $this->province = $province;
        $this->region = $region;
        $this->community = $community;
    }

    public function index($getBy = '', $id = null)
    {
        switch ($getBy) {
            case 'province' :
                return $this->getByProvince($id);
                break;
            case 'region' :
                return $this->getByRegion($id);
                break;
            case 'community' :
                return $this->getByCommunity($id);
                break;
            default:
                return $this->getDefaults();
        }
    }

    public function getProvinces()
    {
        return $this->province->all();
    }

    public function getCommunities($provinceId)
    {
        return $this->community->whereHas('region', function ($region) use ($provinceId) {
            $region->whereHas('province', function ($province) use ($provinceId) {
                $province->where('id', $provinceId);
            });
        })->get();
    }

    private function getDefaults()
    {
        $provinces = $this->province->all();
        $regions = $provinces[0]->regions;
        $communities = $regions[0]->communities;

        return [
            'labels' => [
                'province' => __('places.province'),
                'region' => __('places.region'),
                'community' => __('places.community')
            ],
            'provinces' => $provinces,
            'regions' => $regions,
            'communities' => $communities
        ];
    }

    private function getByProvince($id)
    {
        $regions = $this->region->where('province_id', $id)->get();
        $communities = $regions[0]->communities;

        return [
            'regions' => $regions,
            'communities' => $communities
        ];
    }

    private function getByRegion($id)
    {
        return [
            'communities' => $this->community->where('region_id', $id)->get()
        ];
    }

    private function getByCommunity($id)
    {
        $community = $this->community->find($id);
        $region = $community->region;
        $province = $region->province;

        $provinces = $this->province->all();
        $regions = $province->regions;
        $communities = $region->communities;

        return [
            'places' => [
                'labels' => [
                    'province' => __('places.province'),
                    'region' => __('places.region'),
                    'community' => __('places.community')
                ],
                'provinces' => $provinces,
                'regions' => $regions,
                'communities' => $communities
            ],
            'selected' => [
                'province' => $province->id,
                'region' => $region->id,
                'community' => $community->id
            ]
        ];
    }
}
