<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendApplication;
use App\JobOffer;

class JobOfferController extends Controller
{
    private $jobOffer;

    public function __construct(JobOffer $jobOffer)
    {
        $this->jobOffer = $jobOffer;
    }

    public function index(Request $request)
    {
        return [
            'offers' => $this->jobOffer
                ->when($request->has('employer'), function ($query) use ($request) {
                    $query->where('job_offers.user_id', $request->get('employer'));
                })
                ->when($request->has('kwd'), function ($query) use ($request) {
                    $query->where('job_offers.title', 'like', '%' . $request->get('kwd') . '%');
                })
                ->where(function ($query) use ($request) {
                    if ($request->has('othercountry')) {
                        $query->whereNull('community_id');
                    } else {
                        if ($request->has('community') && $request->get('community') !== '0') {
                            $query->where('community_id', $request->get('community'))->whereNotNull('community_id');
                        } elseif ($request->has('province') && $request->get('province') !== '0') {
                            $query->whereHas('community', function ($community) use ($request) {
                                $community->whereHas('region', function ($region) use ($request) {
                                    $region->whereHas('province', function ($province) use ($request) {
                                        $province->where('id', $request->get('province'));
                                    });
                                });
                            });
                        }
                    }
                })
                ->when($request->has('sphere') && $request->get('sphere') !== 'na', function ($query) use ($request) {
                    $query->where('job_offers.activity_sphere_id', $request->get('sphere'));
                })
                ->when($request->has('education') && $request->get('education') !== 'na', function ($query) use ($request) {
                    $query->where('job_offers.education', $request->get('education'));
                })
                ->when($request->has('experience') && $request->get('experience') !== 'na', function ($query) use ($request) {
                    $query->where('job_offers.experience', $request->get('experience'));
                })
                ->with([
                    'employer' => function ($query) {
                        $query->select(['id'])
                            ->with(['employerDetails' => function ($query) {
                                $query->select(['employer_details.id', 'employer_details.user_id', 'employer_details.name', 'employer_details.logo']);
                            }]);
                    },
                    'community' => function ($query) {
                        $query->with(['region' => function ($query) {
                            $query->with(['province']);
                        }]);
                    },
                    'activitySphere'
                ])
                ->orderBy('id', 'desc')
                ->get()
        ];
    }

    public function applyToOffer($jobOffer, Request $request)
    {
        $jobOffer = $this->jobOffer->findOrFail($jobOffer);
        $jobOffer->load(['employer' => function ($query) {
            $query->select(['users.id'])
                ->with(['employerDetails' => function ($query) {
                    $query->select(['employer_details.user_id', 'employer_details.email']);
                }]);
        }]);

        Mail::to($jobOffer->employer->employerDetails->email)->locale(session('locale'))->send(new SendApplication($jobOffer, $request->user(), $request->get('motivation')));

        return back()->with(['applicationSent' => true]);
    }
}
