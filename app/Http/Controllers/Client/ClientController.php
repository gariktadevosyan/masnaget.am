<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\EmployerDetails;

class ClientController extends Controller
{
    public function index(Request $request)
    {
        $userRole = is_null($request->user()) ? null : $request->user()->role->title;

        return view('client.index', [
            'employers' => EmployerDetails::all(),
            'userRole' => $userRole
        ]);
    }
}
