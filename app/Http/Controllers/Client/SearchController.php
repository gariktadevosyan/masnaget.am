<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ActivitySphere;
use App\CurriculumVitae AS CV;

class SearchController extends Controller
{
    private $activitySphere;
    private $cv;

    public function __construct(ActivitySphere $activitySphere, CV $cv)
    {
        $this->activitySphere = $activitySphere;
        $this->cv = $cv;
    }

    public function searchJobOffers(Request $request)
    {
        return view('search.job_offers', [
            'search' => $request->all(),
            'spheres' => $this->activitySphere->all()
        ]);
    }

    public function searchEmployees(Request $request)
    {
        $cvs = $this->cv
            ->when($request->has('age'), function ($query) use ($request) {
                $age = $request->get('age');
                $year = date("Y");

                if ($age['from']) {
                    $query->where('birth_date', '<=', $year - $age['from'] + 1 . '-01-01');
                }

                if ($age['to']) {
                    $query->where('birth_date', '>=', $year - $age['to']. '-01-01');
                }
            })
            ->where(function ($query) use ($request) {
                if ($request->has('othercountry')) {
                    $query->whereNull('community_id');
                } else {
                    if ($request->has('community') && $request->get('community') !== '0') {
                        $query->where('community_id', $request->get('community'))->whereNotNull('community_id');
                    } elseif ($request->has('province') && $request->get('province') !== '0') {
                        $query->whereHas('community', function ($community) use ($request) {
                            $community->whereHas('region', function ($region) use ($request) {
                                $region->whereHas('province', function ($province) use ($request) {
                                    $province->where('id', $request->get('province'));
                                });
                            });
                        });
                    }
                }
            })
            ->when($request->has('experience'), function ($query) use ($request) {
                $experience = $request->get('experience');

                if ($experience['sphere'] !== 'na' || $experience['period'] !== 'na') {
                    $query->whereHas('spheres', function ($query) use ($experience) {
                        if ($experience['sphere'] !== 'na') {
                            $query->where('activity_sphere_id', $experience['sphere']);
                        }

                        if ($experience['period'] !== 'na') {
                            $query->where('experience', $experience['period']);
                        }
                    });
                }
            })
            ->when($request->has('education') && $request->get('education') !== 'na', function ($query) use ($request) {
                $query->where('education', $request->get('education'));
            })
            ->get(['id', 'name', 'birth_date']);

        return view('search.employees', [
            'cvs' => $cvs,
            'search' => $request->all(),
            'spheres' => $this->activitySphere->all()
        ]);
    }
}
