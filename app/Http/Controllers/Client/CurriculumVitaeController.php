<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCVRequest;
use App\ActivitySphere;
use Illuminate\Support\Facades\Storage;
use App\CurriculumVitae AS CV;

class CurriculumVitaeController extends Controller
{
    private $activitySphere;
    private $cv;

    public function __construct(ActivitySphere $activitySphere, CV $cv)
    {
        $this->activitySphere = $activitySphere;
        $this->cv = $cv;
    }

    public function index(Request $request)
    {
        $user = $request->user()->load(['cv']);

        $cv = $user->cv ? $user->cv->load(['spheres', 'experiences']) : null;
        $spheresSetList = !is_null($cv) && !is_null($cv->spheres) ? $this->getSpheresSetList($cv->spheres) : null;

        return view('client.cv.index', [
            'user' => $user,
            'cv' => $user->cv,
            'spheresSetList' => $spheresSetList,
            'spheres' => $this->activitySphere->all()
        ]);
    }

    public function store(StoreCVRequest $request)
    {
        $user = $request->user();
        $cv = $user->cv()->firstOrNew([]);

        $fillable = $request->except(['_token', 'spheres', 'experience']);

        if ($request->has('avatar')) {
            $avatar = $request->input('avatar');
            if ($avatar === 'DELETE') {
                $this->removeAvatar($cv->getOriginal('avatar'));
                $fillable['avatar'] = null;
            } else {
                if ($cv->exists) {
                    $this->removeAvatar($cv->getOriginal('avatar'));
                }
                $fillable['avatar'] = $this->storeAvatar($user->id, $avatar);
            }
        }

        $fillable['community_id'] = $request->input('community_id', null);
        $fillable['education_details'] = $request->input('education_details', null);

        $cv->fill($fillable);
        $cv->save();

        $spheres = $request->input('spheres', []);
        $spheresToSync = [];

        if (!empty($spheres)) {
            foreach ($spheres as $sphere) {
                $spheresToSync[$sphere['sphere']] = ['experience' => $sphere['experience']];
            }
        }

        $cv->spheres()->sync($spheresToSync);

        $cv->experiences()->createMany($request->input('experience', []));

        return back()->with(['saved' => true]);
    }

    public function show($id)
    {
        return view('client.cv.show', [
            'cv' => $this->cv->find($id)->load('user')
        ]);
    }

    private function storeAvatar($userId, $avatar)
    {
        $avatar = explode(',', $avatar);
        $fileName = $this->generateFileName($userId);
        Storage::put('avatars/'. $fileName .'.png', base64_decode($avatar[1]));
        return $fileName;
    }

    private function removeAvatar($fileName)
    {
        Storage::delete('avatars/'. $fileName .'.png');
    }

    private function getSpheresSetList($spheres)
    {
        if (!$spheres){
            return [];
        }

        return $spheres->map(function ($sphere) {
            return [
                'sphere' => $sphere->pivot->activity_sphere_id,
                'experience' => $sphere->pivot->experience
            ];
        });
    }
}
