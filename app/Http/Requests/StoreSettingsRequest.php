<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreSettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isRole('employer');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'logo' => 'bail|required_if:_method,POST|string',
            'name' => 'bail|required|string|max:50',
            'activity_sphere_id' => 'bail|required|numeric|exists:activity_spheres,id',
            'tin' => [
                'bail',
                'required',
                'alpha_num',
                'size:8',
                Rule::unique('employer_details')->ignore($this->user()->employerDetails()->first(['id'])),
            ],
            'employees_count' => 'bail|nullable|numeric|min:0',
            'community_id' => 'bail|numeric|exists:communities,id',
            'address' => 'bail|nullable|string|required_without:community_id',
            'phone' => 'nullable|array',
            'email' => 'required|email',
            'website' => 'nullable|string|max:50',
            'agent' => 'nullable|string|max:50',
            'about' => 'nullable|string',
        ];
    }
}
