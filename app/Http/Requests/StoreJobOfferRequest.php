<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreJobOfferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isRole('employer');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:100',
            'activity_sphere_id' => 'bail|required|numeric|exists:activity_spheres,id',
            'description' => 'nullable|string',
            'term' => 'bail|required|numeric|in:0,1,2',
            'type' => 'bail|required|numeric|in:0,1,2',
            'community_id' => 'bail|numeric|exists:communities,id',
            'address' => 'bail|nullable|string|required_without:community_id',
            'opening_date' => 'bail|required|date_format:' . config('app.date_format'),
            'deadline' => 'bail|required|date_format:' . config('app.date_format') . '|after_or_equal:opening_date',
            'languages' => 'required|array',
            'experience' => 'bail|required|numeric|in:0,1,2',
            'military_book' => 'bail|required|numeric|in:0,1,2',
            'drive_right' => 'bail|required|numeric|in:0,1,2',
            'education' => 'bail|required|numeric|in:0,1,2',
            'skills' => 'nullable|string'
        ];
    }
}
