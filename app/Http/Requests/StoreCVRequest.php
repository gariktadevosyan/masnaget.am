<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCVRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avatar' => 'string',
            'name' => 'required|string|max:50',
            'birth_date' => 'required|date_format:' . config('app.date_format') . '|before:today',
            'community_id' => 'numeric|exists:communities,id',
            'address' => 'nullable|string|max:50|required_without:community_id',
            'education' => 'required|numeric|in:0,1,2',
            'education_details' => 'array',
            'languages' => 'required|array',
            'spheres' => 'array',
            'experience' => 'array',
            'skills' => 'nullable|string',
            'military_book' => 'string',
            'drive_right' => 'string',
        ];
    }
}
