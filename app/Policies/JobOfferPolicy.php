<?php

namespace App\Policies;

use App\User;
use App\JobOffer;
use Illuminate\Auth\Access\HandlesAuthorization;

class JobOfferPolicy
{
    use HandlesAuthorization;

    public function view(?User $user, JobOffer $jobOffer)
    {
        return true;
    }

    public function create(User $user)
    {
        return $user->isRole('employer');
    }

    public function update(User $user, JobOffer $jobOffer)
    {
        return $jobOffer->user_id === $user->id;
    }

    public function delete(User $user, JobOffer $jobOffer)
    {
        return $jobOffer->user_id === $user->id;
    }

    public function restore(User $user, JobOffer $jobOffer)
    {
        return $jobOffer->user_id === $user->id;
    }
}
