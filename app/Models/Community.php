<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Community extends Model
{
    public $timestamps = false;
    protected $fillable = ['name'];

    public function region()
    {
        return $this->belongsTo('App\Region');
    }

    public function employerDetails()
    {
        return $this->hasMany('App\EmployerDetails');
    }

    public function getNameAttribute($value)
    {
        return __('places.' . $value);
    }
}
