<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    public $timestamps = false;
    protected $fillable = ['name'];

    public function regions()
    {
        return $this->hasMany('App\Region');
    }

    public function getNameAttribute($value)
    {
        return __('places.' . $value);
    }
}
