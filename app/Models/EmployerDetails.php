<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployerDetails extends Model
{
    public $timestamps = false;
    protected $guarded = ['id', 'user_id'];
    protected $casts = [
        'phone' => 'array'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function activitySphere()
    {
        return $this->belongsTo('App\ActivitySphere');
    }

    public function community()
    {
        return $this->belongsTo('App\Community');
    }

    public function getLogoAttribute($value)
    {
        return asset('storage/logos/' . $value . '.png');
    }

    public function getLocationAttribute()
    {
        $location = '';
        $hasAddress = !is_null($this->address);

        if (!is_null($this->community_id)) {
            $location .= $this->community->region->province->name . ', ' . $this->community->region->name . ', ' . $this->community->name;

            if ($hasAddress) {
                $location .= ', ';
            }
        }

        if ($hasAddress) {
            $location .= $this->address;
        }

        return $location;
    }
}
