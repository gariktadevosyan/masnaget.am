<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    protected $guarded = ['id', 'cv_id'];
    public $timestamps = false;

    public function cv()
    {
        return $this->belongsTo('App\CurriculumVitae', 'cv_id');
    }

    public function sphere()
    {
        return $this->belongsTo('App\ActivitySphere', 'sphere_id');
    }

    public function setStartDateAttribute($value)
    {
        $this->attributes['start_date'] = date('Y-m-d', strtotime('01.' . $value));
    }

    public function setEndDateAttribute($value)
    {
        $this->attributes['end_date'] = date('Y-m-d', strtotime('01.' . $value));
    }

    public function getStartDateAttribute($value)
    {
        return date('m.Y', strtotime($value));
    }

    public function getEndDateAttribute($value)
    {
        return date('m.Y', strtotime($value));
    }
}
