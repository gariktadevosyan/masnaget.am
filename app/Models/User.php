<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    protected $fillable = [
        'email', 'password', 'role_id',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function isRole($role)
    {
        return $this->role->title === $role;
    }

    public function jobOffers()
    {
        return $this->hasMany('App\JobOffer');
    }

    public function employerDetails()
    {
        return $this->hasOne('App\EmployerDetails');
    }

    public function cv()
    {
        return $this->hasOne('App\CurriculumVitae');
    }

    public function hasCV()
    {
        return $this->cv()->count() > 0;
    }
}
