<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivitySphere extends Model
{
    public $timestamps = false;
    protected $fillable = ['title'];

    public function getTitleAttribute($value)
    {
        return __('activity_spheres.' . $value);
    }

    public function employerDetails()
    {
        return $this->hasMany('App\EmployerDetails');
    }

    public function CVs()
    {
        return $this->belongsToMany('App\CurriculumVitae');
    }

    public function experiences()
    {
        return $this->hasMany('App\Experience', 'sphere_id');
    }
}
