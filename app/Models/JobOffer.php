<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobOffer extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $guarded = ['id', 'user_id'];

    protected $casts = [
        'languages' => 'array'
    ];

    protected $appends = ['location'];

    public function employer()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function activitySphere()
    {
        return $this->belongsTo('App\ActivitySphere');
    }

    public function community()
    {
        return $this->belongsTo('App\Community');
    }

    public function getLocationAttribute()
    {
        $location = '';
        $hasAddress = !is_null($this->address);

        if (!is_null($this->community_id)) {
            $location .= $this->community->region->province->name . ', ' . $this->community->region->name . ', ' . $this->community->name;

            if ($hasAddress) {
                $location .= ', ';
            }
        }

        if ($hasAddress) {
            $location .= $this->address;
        }

        return $location;
    }

    public function getOpeningDateAttribute($value)
    {
        if (is_null($value))
        {
            return $value;
        }

        return date(config('app.date_format'), strtotime($value));
    }

    public function setOpeningDateAttribute($value)
    {
        if (is_null($value)) {
            $this->attributes['opening_date'] = null;
        } else {
            $this->attributes['opening_date'] = date('Y-m-d', strtotime($value));
        }
    }
    public function getDeadlineAttribute($value)
    {
        if (is_null($value))
        {
            return $value;
        }

        return date(config('app.date_format'), strtotime($value));
    }

    public function setDeadlineAttribute($value)
    {
        if (is_null($value)) {
            $this->attributes['deadline'] = null;
        } else {
            $this->attributes['deadline'] = date('Y-m-d', strtotime($value));
        }
    }

    public function getUpdatedAtAttribute($value)
    {
        return date(config('app.date_format'), strtotime($value));
    }

    public function getCreatedAtAttribute($value)
    {
        return date(config('app.date_format'), strtotime($value));
    }

    public function getDeletedAtAttribute($value)
    {
        if (is_null($value))
        {
            return $value;
        }

        return date(config('app.date_format'), strtotime($value));
    }
}
