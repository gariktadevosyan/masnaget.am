<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    public $timestamps = false;
    protected $fillable = ['name'];

    public function province()
    {
        return $this->belongsTo('App\Province');
    }

    public function communities()
    {
        return $this->hasMany('App\Community');
    }

    public function getNameAttribute($value)
    {
        return __('places.' . $value);
    }
}
