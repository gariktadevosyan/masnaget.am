<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurriculumVitae extends Model
{
    protected $guarded = ['id', 'user_id'];
    protected $casts = [
        'phone' => 'array',
        'education_details' => 'array',
        'languages' => 'array',
        'military_book' => 'boolean',
        'drive_right' => 'boolean'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function experiences()
    {
        return $this->hasMany('App\Experience', 'cv_id');
    }

    public function getBirthDateAttribute($value)
    {
        return date(config('app.date_format'), strtotime($value));
    }

    public function setBirthDateAttribute($value)
    {
        $this->attributes['birth_date'] = date('Y-m-d', strtotime($value));
    }

    public function spheres()
    {
        return $this->belongsToMany('App\ActivitySphere')->withPivot(['experience']);
    }

    public function getAvatarAttribute($value)
    {

        return is_null($value) ? null : asset('storage/avatars/' . $value . '.png');
    }

    public function setMilitaryBookAttribute($value)
    {
        $this->attributes['military_book'] = boolval($value);
    }

    public function setDriveRightAttribute($value)
    {
        $this->attributes['drive_right'] = boolval($value);
    }

    public function community()
    {
        return $this->belongsTo('App\Community');
    }

    public function getLanguagesAsStringAttribute()
    {
        return implode(', ', array_map(function ($lang) {
            return __('options.languages.' . $lang);
        }, $this->languages));
    }
}
