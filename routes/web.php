<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['locale'])->group(function () {
    Route::get('/offer/{job_offer}', 'Admin\JobOfferController@show')->name('offer.show');
    Route::get('/employer/{employer_details}', 'Admin\SettingsController@show');

    Route::namespace('Client')->group(function () {
        Route::get('/', 'ClientController@index');
        Route::get('/search', 'SearchController@searchJobOffers');
        Route::get('/search/cv', 'SearchController@searchEmployees')->middleware('auth', 'admin');
        Route::get('/cv/{id}', 'CurriculumVitaeController@show')->middleware('auth', 'admin');

        Route::middleware(['auth', 'verified', 'client'])->group(function () {
            //    CV
            Route::get('/cv', 'CurriculumVitaeController@index')->name('cv.index');
            Route::post('/cv', 'CurriculumVitaeController@store')->name('cv.store');
        });
    });

    //api
    Route::namespace('Api')->prefix('api')->group(function () {
        Route::get('/job-offers', 'JobOfferController@index');
        Route::get('/places/provinces', 'PlaceController@getProvinces');
        Route::get('/places/communities/{province}', 'PlaceController@getCommunities');

        Route::middleware(['auth'])->group(function () {
            Route::post('/job-offer/apply/{job_offer}', 'JobOfferController@applyToOffer');
            Route::get('/places/{getBy?}/{id?}', 'PlaceController@index');
        });
    });

    Auth::routes(['verify' => true]);
});

Route::namespace('Admin')->middleware(['auth', 'admin'])->group(function () {
    Route::get('/questionary', 'SettingsController@showQuestionary');

    Route::prefix('admin')->middleware(['verified'])->group(function () {
        Route::get('details', 'DetailsController');
        Route::post('settings', 'SettingsController@store')->name('settings.store');

        Route::middleware(['details'])->group(function () {
            Route::get('/', 'DashboardController@index')->name('admin.index');

            //    settings
            Route::put('settings', 'SettingsController@update')->name('settings.update');
            Route::get('settings', 'SettingsController@index')->name('settings.index');

//        job offers
            Route::get('job-offers/archive', 'JobOfferController@archive')->name('job-offers.archive');
            Route::patch('job-offers/{job_offer}/restore', 'JobOfferController@restore')->name('job-offers.restore');

            Route::resources([
                'job-offers' => 'JobOfferController'
            ]);
        });
    });
});



Route::get('/locale/{locale}', function ($locale) {
    session(['locale' => $locale]);
    return back();
})->name('locale');

